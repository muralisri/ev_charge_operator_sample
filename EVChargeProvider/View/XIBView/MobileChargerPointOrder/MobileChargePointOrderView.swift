//
//  MobileChargePointOrderView.swift
//  EVChargeProvider
//
//  Created by MURALI on 10/01/22.
//

import UIKit
import SideMenu

class MobileChargePointOrderView: UIView {

    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var carModelView: UIView!
    @IBOutlet weak var chargeStandardView: UIView!
    @IBOutlet weak var LicensePlateView: UIView!
    @IBOutlet weak var phoneNoView: UIView!
    @IBOutlet weak var estimatekWhView: UIView!
    @IBOutlet weak var costView: UIView!
    
    @IBOutlet weak var mobileChargeView: UIView!
    @IBOutlet weak var mobileChargeOrderView: UIView!
    
    
    @IBOutlet weak var requestLocation: UILabel!
    @IBOutlet weak var carModel: UILabel!
    @IBOutlet weak var chargeStandard: UILabel!
    @IBOutlet weak var licensePlateNo: UILabel!
    @IBOutlet weak var requestPhoneNo: UILabel!
    @IBOutlet weak var estimatedkWh: UILabel!
    @IBOutlet weak var estimatedCost: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var chargeOrderDetailsHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var mobileChargeOrderViewHeightConstrain: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setDesignStyle()
        
      // mobileChargeOrderViewHeightConstrain.constant = 0
        //chargeOrderDetailsHeightConstrain.constant = 0
//        setViewAnimation()

        
    }
    
    
    
    func setDesignStyle() {
        
        locationView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        carModelView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        chargeStandardView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        LicensePlateView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        phoneNoView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        estimatekWhView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        costView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        
        mobileChargeView.setViewCornerWithRadiusAndNoBorder(radius: 20)
        mobileChargeOrderView.setViewCornerWithRadiusAndNoBorder(radius: 20)
        
        confirmButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        cancelButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
    }
    
    func setViewAnimation() {
        
        self.transform = CGAffineTransform(scaleX: 0.3, y: 2)

        UIView.animate(withDuration: 0.5, delay: 0, options: [.allowUserInteraction]) {
            self.transform = .identity
            self.mobileChargeOrderViewHeightConstrain.constant = 620
            self.layoutIfNeeded()
        } completion: { _ in
            
        }

        self.alpha = 1


    }
    
    
}
