//
//  SearchLocationTableViewCell.swift
//  EVChargeProvider
//
//  Created by MURALI on 25/01/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class SearchLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var primaryAddress: UILabel!
    @IBOutlet weak var secondaryAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
