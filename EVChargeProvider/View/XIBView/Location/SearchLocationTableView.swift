//
//  SearchLocationView.swift
//  EVChargeProvider
//
//  Created by MURALI on 25/01/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SearchLocationTableView: UIView {

    @IBOutlet weak var locationTableView: UITableView!
    
    
   // var dataSource = [String]()
    
    var dataSource = [GMSAutocompletePrediction]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        locationTableView.delegate = self
        locationTableView.dataSource = self
        
        let nib = UINib(nibName: "SearchLocationTableViewCell", bundle: nil)
        locationTableView.register(nib, forCellReuseIdentifier: "LocationCell")
        
        locationTableView.reloadData()
        
        
        
    }
    
    func getLocation(source: [GMSAutocompletePrediction]) {
        print("GetLocation \(source)")
        dataSource = source
        self.locationTableView.reloadData()
    }

}

extension SearchLocationTableView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Source \(dataSource.count)")
        
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as? SearchLocationTableViewCell
        
        cell?.primaryAddress.text = dataSource[indexPath.row].attributedFullText.string
        
        
        return cell!
    }
}
