//
//  FilterMapView.swift
//  EVChargeProvider
//
//  Created by MURALI on 11/01/22.
//

import UIKit

class FilterMapView: UIView {

    @IBOutlet weak var standardButton: UIButton!
    @IBOutlet weak var satelliteButton: UIButton!
    @IBOutlet weak var hybridButton: UIButton!
    
    let greenColor = UIColor(red: 38/255, green: 217/255, blue: 117/255, alpha: 1)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialLoad()
        setDesignStyle()
        
        hybridButton.backgroundColor = greenColor
        
    }
    
    func initialLoad() {
        
        standardButton.addTarget(self, action: #selector(standardButtonAction), for: .touchUpInside)
        satelliteButton.addTarget(self, action: #selector(satelliteButtonAction), for: .touchUpInside)
        hybridButton.addTarget(self, action: #selector(hybridButtonAction), for: .touchUpInside)
    }
    
    func setDesignStyle() {
        
        standardButton.setButtonRoundCornerWithBorder()
        satelliteButton.setButtonRoundCornerWithBorder()
        hybridButton.setButtonRoundCornerWithBorder()
    }
    
    @objc func standardButtonAction() {
        standardButton.backgroundColor = greenColor
        satelliteButton.backgroundColor = .white
        hybridButton.backgroundColor = .white
        
    }
    @objc func satelliteButtonAction() {
        
        standardButton.backgroundColor = .white
        satelliteButton.backgroundColor = greenColor
        hybridButton.backgroundColor = .white
    }
    
    @objc func hybridButtonAction() {
        standardButton.backgroundColor = .white
        satelliteButton.backgroundColor = .white
        hybridButton.backgroundColor = greenColor
    }

}
