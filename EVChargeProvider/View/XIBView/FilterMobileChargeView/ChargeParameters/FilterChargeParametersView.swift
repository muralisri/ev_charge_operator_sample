//
//  ChargeParametersView.swift
//  EVChargeProvider
//
//  Created by MURALI on 11/01/22.
//

import UIKit

class FilterChargeParametersView: UIView {

    @IBOutlet weak var requiredEnergyLable: UILabel!
    @IBOutlet weak var requiredEnergySlider: UISlider!
    @IBOutlet weak var requiredEnergySwitch: UISwitch!
    @IBOutlet weak var requiredEnergySliderValue: UILabel!
    
    @IBOutlet weak var estimatedCostLable: UILabel!
    @IBOutlet weak var estimatedCostSlider: UISlider!
    @IBOutlet weak var estimatedCostSwitch: UISwitch!
    @IBOutlet weak var estimatedCostSliderValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        onClickButtonAction()
    }
    
    func onClickButtonAction() {
        requiredEnergySlider.addTarget(self, action: #selector(equiredEnergySliderAction(_:)), for: .valueChanged)
        estimatedCostSlider.addTarget(self, action: #selector(estimatedCostSliderAction(_:)), for: .valueChanged)
    }
    
    @objc func equiredEnergySliderAction(_ slider: UISlider) {
        
        let value = slider.value
        requiredEnergySliderValue.text = "kWh \(Int(value))"
        
    }
    
    @objc func estimatedCostSliderAction(_ slider: UISlider) {
        
        let value = slider.value
        estimatedCostSliderValue.text = "UGX \(Int(value))"
    }
    
}
