//
//  DistanceView.swift
//  EVChargeProvider
//
//  Created by MURALI on 11/01/22.
//

import UIKit

class FilterDistanceView: UIView {

    @IBOutlet var kmButtonColletion: [UIButton]!
    
    @IBOutlet var kmLabelCollection: [UILabel]!
    
    
    let greenColor = UIColor(red: 38/255, green: 217/255, blue: 117/255, alpha: 1)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setDesignStyle()
        initialLoad()
        kmButtonColletion[3].backgroundColor = greenColor
        kmLabelCollection[3].textColor = greenColor
    }
    
    func initialLoad() {
        
        
        kmButtonColletion[0].addTarget(self, action: #selector(km50ButtonsAction), for: .touchUpInside)
        kmButtonColletion[1].addTarget(self, action: #selector(km40ButtonsAction), for: .touchUpInside)
        kmButtonColletion[2].addTarget(self, action: #selector(km30ButtonsAction), for: .touchUpInside)
        kmButtonColletion[3].addTarget(self, action: #selector(km20ButtonsAction), for: .touchUpInside)
        kmButtonColletion[4].addTarget(self, action: #selector(km10ButtonsAction), for: .touchUpInside)
        
       
        
    }
    
    func setDesignStyle() {
        
        // km Button Colletion
        for button in kmButtonColletion {
            
            button.setButtonRoundCornerWithShadow(style: .round, radius: 0)
        }
        
    }
    
    
    
    @objc func km50ButtonsAction() {
        
        kmButtonColletion[0].backgroundColor = greenColor
        kmButtonColletion[1].backgroundColor = .white
        kmButtonColletion[2].backgroundColor = .white
        kmButtonColletion[3].backgroundColor = .white
        kmButtonColletion[4].backgroundColor = .white
        
        //km Label Colletion
        kmLabelCollection[0].textColor = greenColor
        kmLabelCollection[1].textColor = .darkGray
        kmLabelCollection[2].textColor = .darkGray
        kmLabelCollection[3].textColor = .darkGray
        kmLabelCollection[4].textColor = .darkGray
   
    }
    @objc func km40ButtonsAction() {
        
        kmButtonColletion[0].backgroundColor = .white
        kmButtonColletion[1].backgroundColor = greenColor
        kmButtonColletion[2].backgroundColor = .white
        kmButtonColletion[3].backgroundColor = .white
        kmButtonColletion[4].backgroundColor = .white
        
        //km Label Colletion
        kmLabelCollection[0].textColor = .darkGray
        kmLabelCollection[1].textColor = greenColor
        kmLabelCollection[2].textColor = .darkGray
        kmLabelCollection[3].textColor = .darkGray
        kmLabelCollection[4].textColor = .darkGray
        
        
        
    }
    @objc func km30ButtonsAction() {
        
        kmButtonColletion[0].backgroundColor = .white
        kmButtonColletion[1].backgroundColor = .white
        kmButtonColletion[2].backgroundColor = greenColor
        kmButtonColletion[3].backgroundColor = .white
        kmButtonColletion[4].backgroundColor = .white
        
        //km Label Colletion
        kmLabelCollection[0].textColor = .darkGray
        kmLabelCollection[1].textColor = .darkGray
        kmLabelCollection[2].textColor = greenColor
        kmLabelCollection[3].textColor = .darkGray
        kmLabelCollection[4].textColor = .darkGray
        
    }
    @objc func km20ButtonsAction() {
        
        kmButtonColletion[0].backgroundColor = .white
        kmButtonColletion[1].backgroundColor = .white
        kmButtonColletion[2].backgroundColor = .white
        kmButtonColletion[3].backgroundColor = greenColor
        kmButtonColletion[4].backgroundColor = .white
        
        //km Label Colletion
        kmLabelCollection[0].textColor = .darkGray
        kmLabelCollection[1].textColor = .darkGray
        kmLabelCollection[2].textColor = .darkGray
        kmLabelCollection[3].textColor = greenColor
        kmLabelCollection[4].textColor = .darkGray
        
    }
    @objc func km10ButtonsAction() {
        
        kmButtonColletion[0].backgroundColor = .white
        kmButtonColletion[1].backgroundColor = .white
        kmButtonColletion[2].backgroundColor = .white
        kmButtonColletion[3].backgroundColor = .white
        kmButtonColletion[4].backgroundColor = greenColor
        
        //km Label Colletion
        kmLabelCollection[0].textColor = .darkGray
        kmLabelCollection[1].textColor = .darkGray
        kmLabelCollection[2].textColor = .darkGray
        kmLabelCollection[3].textColor = .darkGray
        kmLabelCollection[4].textColor = greenColor
        
    }
}
