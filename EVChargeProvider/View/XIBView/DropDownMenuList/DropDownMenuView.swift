//
//  DropDownMenuView.swift
//  EVChargeProvider
//
//  Created by MURALI on 24/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class DropDownMenuView: UIView {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var dropDownMenuTableView: UITableView!
    
    var selectedValue: ((String?, Bool)-> Void)?
    
    var currencyList = ["UGX", "GHS", "KES", "RWF", "NGN", "TZS", "SLL", "ZMW", "ZAR", "USD", "EUR", "GBP", "XOF", "XAF", "ETB"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dropDownMenuTableView.delegate = self
        dropDownMenuTableView.dataSource = self
        dropDownMenuTableView.separatorStyle = .none
        
        let nib = UINib(nibName: "DropDownMenuTableViewCell", bundle: nil)
        dropDownMenuTableView.register(nib, forCellReuseIdentifier: "DropDownCell")
        
        selectedValue?(nil, false)
        
    }

}

extension DropDownMenuView: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell") as? DropDownMenuTableViewCell
        cell?.nameLabel.text = currencyList[indexPath.row]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        let selectedText = currencyList[index]
        
        selectedValue?(selectedText, true)
        
        print("Selected value && \(selectedText)")
    }
}
