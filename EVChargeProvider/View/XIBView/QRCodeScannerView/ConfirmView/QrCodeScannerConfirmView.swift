//
//  QrCodeScannerConfirmView.swift
//  EVChargeProvider
//
//  Created by MURALI on 20/01/22.
//

import UIKit


class QrCodeScannerConfirmView: UIView {
    @IBOutlet weak var detailsHoldingView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setDesignStyle()
    }

    func setDesignStyle() {
        
        detailsHoldingView.setViewCornerWithRadiusAndNoBorder(radius: 20)
        
        confirmButton.setButtonRoundCornerWithShadow(style: .normal, radius: 5)
        titleLabel.setLableSomeCornerRadius(radius: 20)
    }
    
}
