//
//  QrCodeScannerEnterPointIdView.swift
//  EVChargeProvider
//
//  Created by MURALI on 20/01/22.
//

import UIKit

class QrCodeScannerEnterPointIdView: UIView {

    @IBOutlet weak var detailsHoldingView: UIView!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var enterPointId: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setDesignStyle()
    }

    
    func setDesignStyle() {
        
        detailsHoldingView.setViewCornerWithRadiusAndNoBorder(radius: 20)
        titleLable.setLableSomeCornerRadius(radius: 20)
        submitButton.setViewCornerWithRadiusAndNoBorder(radius: 5)
        
        
    }
}
