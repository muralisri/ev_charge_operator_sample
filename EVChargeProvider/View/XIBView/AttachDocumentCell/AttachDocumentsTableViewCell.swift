//
//  AttachDocumentsTableViewCell.swift
//  EVChargeProvider
//
//  Created by MURALI on 19/01/22.
//

import UIKit

class AttachDocumentsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imageUploaderView: UIView!
    @IBOutlet weak var identityName: UILabel!
    @IBOutlet weak var identityDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
