//
//  MobileChargePointEnroute.swift
//  EVChargeProvider
//
//  Created by MURALI on 10/01/22.
//

import UIKit

class MobileChargePointEnroute: UIView {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var arriveInView: UIView!
    @IBOutlet weak var requestorView: UIView!
    
    @IBOutlet weak var phoneCallButton: UIStackView!
    @IBOutlet weak var inAppcallButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var telephoneNumer: UILabel!
    
    @IBOutlet weak var confirmedOrderLabel: UILabel!
    
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainViewHeightConstrain: NSLayoutConstraint!
    
    var mobileChargePointEnroute: MobileChargePointEnroute?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDesignStyle()
        initialLoad()
       // mainViewHeightConstrain.constant = 0
    }

    func setDesignStyle() {
        topView.setViewCornerWithRadiusAndNoBorder(radius: 30)
        bottomView.setViewCornerWithRadiusAndNoBorder(radius: 30)
        arriveInView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        requestorView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        
        phoneCallButton.setViewCornerWithRadiusAndNoBorder(radius: 5)
        inAppcallButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        cancelButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        
        confirmedOrderLabel.setLabelCornerRadius(radius: 7)
    }
    
    func initialLoad() {
        
        
    }
    
    
}
