//
//  ChargingReceiptView.swift
//  EVChargeProvider
//
//  Created by MURALI on 10/01/22.
//

import UIKit

class ChargingReceiptView: UIView {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var profileHoldingView: UIView!
    @IBOutlet weak var topHoldingView: UIView!
    @IBOutlet weak var bottomHoldingView: UIView!
    
    @IBOutlet weak var exportButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileHoldingView.setViewCornerRadiusWithBorder(radius: 50, width: 3, color: .green)
        bottomHoldingView.setViewCornorRadiusWithShadow(radius: 40)
        
       // bottomHoldingView.setViewCornerWithRadiusAndNoBorder(radius: 40)
        
        
//        bottomHoldingView.layer.shadowColor = UIColor.darkGray.cgColor
//        bottomHoldingView.layer.shadowRadius = 5
//        bottomHoldingView.layer.shadowOpacity = 2
//        bottomHoldingView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        //exportButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
//        exportButton.layer.shadowColor = UIColor.darkGray.cgColor
//        exportButton.layer.shadowRadius = 2
//        exportButton.layer.shadowOpacity = 2
//        exportButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        exportButton.setButtonRoundCornerWithShadow(style: .normal, radius: 7)
    }

}
