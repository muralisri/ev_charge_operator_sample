//
//  PaymentMethodCollectionViewCell.swift
//  EVChargeProvider
//
//  Created by MURALI on 21/01/22.
//

import UIKit

class PaymentMethodCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var payModeCellView: UIView!
    @IBOutlet weak var payModeImage: UIImageView!
    
    var isChecked: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        backgroundView?.setViewCornerRadiusWithBorder(radius: 20, width: 2, color: .orange)
        contentView.setViewCornerRadiusWithBorder(radius: 20, width: 2, color: .orange)
        
    }

}
