//
//  AddCardCollectionViewCell.swift
//  EVChargeProvider
//
//  Created by MURALI on 21/01/22.
//

import UIKit

class AddCardCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var addCardCellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.setViewCornerWithRadiusAndNoBorder(radius: 7)
    }

}
