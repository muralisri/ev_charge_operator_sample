//
//  CommonResource.swift
//  EVChargeProvider
//
//  Created by MURALI on 02/03/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import Foundation

class Resource {
    static let currencyList = ["NGN", "GHS", "KES", "RWF", "UGX", "TZS", "SLL", "ZMW", "ZAR", "USD", "EUR", "GBP", "XOF", "XAF", "ETB"]
    static let operatorTyps = ["Mobile", "Fixed"]
    
}

