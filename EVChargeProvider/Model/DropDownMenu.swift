//
//  DropDownMenu.swift
//  EVChargeProvider
//
//  Created by MURALI on 28/01/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import Foundation
import UIKit

class DropDownMenu {
    
    @available(iOS 14.0, *)
    class func configureDropDownMenu(dataSource: [String], menuTitle: String, field: UIButton, completion: @escaping ((String)-> Void)) {

        var menuLists = [UIMenuElement]()
        
        for dataSource in dataSource {
            
            menuLists.append(UIAction(title: dataSource, handler: { source in
                
                field.setTitle(dataSource, for: .normal)
                completion(dataSource)
                
            }))
            
            let menu = UIMenu(title: menuTitle, children: menuLists)
            field.showsMenuAsPrimaryAction = true
            field.menu = menu
        }
  
    }
}
