//
//  ButtonExtension.swift
//  EVChargeProvider
//
//  Created by MURALI on 06/01/22.
//

import Foundation
import UIKit

enum ButtonStyle {
    case normal
    case round
}

extension UIButton {
    
   
    
    func setButtonCornorWithRadiusAndNoBorder(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
    
    func setButtonCornorRadiusWithGreenBorder(radius: CGFloat, width: CGFloat) {
        self.layer.borderColor = CGColor(red: 38/255, green: 217/255, blue: 117/255, alpha: 1)
        self.layer.cornerRadius = radius
        self.layer.borderWidth = width
    }
    
    func setButtonRoundCornerWithBorder() {
        self.layer.cornerRadius = self.frame.height/2
        self.layer.borderColor = CGColor(red: 175/255, green: 174/255, blue: 175/255, alpha: 1)
        self.layer.borderWidth = 2
        
    }
    
    func setButtonRoundCornerWithShadow(style: ButtonStyle, radius: CGFloat) {
        
      
        switch style {
            
        case .normal:
            self.layer.cornerRadius = radius
           
        case .round:
            
            self.layer.cornerRadius = self.frame.height/2
        }
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
    }
}
