//
//  ImageExtension.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import Foundation
import UIKit

extension UIImageView {
    
    func setImageCornorRoundWithGreenBorder(width: CGFloat) {
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.borderWidth = width
        self.layer.masksToBounds = true
        self.layer.borderColor = CGColor(red: 38/255, green: 217/255, blue: 117/255, alpha: 1)
    }
    
    func setImageCornorRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
    }
}
