//
//  LabelExtension.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import Foundation
import UIKit

class mm : UILabel {
  
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: 0, left: 5  , bottom: 0, right: 5)
        super.drawText(in: rect.inset(by: insets))
    }

}

extension UILabel {
   
    func setLabelCornerRadiusWithGreenBorder(radius: CGFloat, width: CGFloat) {
        
        self.layer.borderColor = CGColor(red: 38/255, green: 217/255, blue: 117/255, alpha: 1)
        self.layer.borderWidth = width
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        
      
        
    }
    
    
    
    func setLabelCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        
    }
    
    func setLableSomeCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    
    
}
