//
//  ViewControllerExtension.swift
//  EVChargeProvider
//
//  Created by MURALI on 13/01/22.
//

import Foundation
import UIKit


extension UIViewController {
    
    func dismiss(animated: Bool) {
        
        if navigationController != nil {
            navigationController?.popViewController(animated: animated)
        } else {
            dismiss(animated: animated, completion: nil)
        }
    }
    
    func push(controller: String, animated: Bool) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: controller)
        navigationController?.pushViewController(vc!, animated: animated)
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
