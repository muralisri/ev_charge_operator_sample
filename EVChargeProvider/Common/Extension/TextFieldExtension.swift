//
//  TextFieldExtension.swift
//  EVChargeProvider
//
//  Created by MURALI on 06/01/22.
//

import Foundation
import UIKit



extension UITextField {
    
    enum Color {
        case green
        case orange
    }
    
    func setTextFieldCornorWithRadiusAndNoBorder(radius: CGFloat) {
        
        self.layer.cornerRadius = radius
    }
    
    func setTextFieldCornorRadiusWithBorder(radius: CGFloat, width: CGFloat, color: Color) {
        
        switch color {
        case .green:
            self.layer.borderColor = CGColor(red: 38/255, green: 217/255, blue: 117/255, alpha: 1)
        case .orange:
            self.layer.borderColor = CGColor(red:255/255, green: 105/255, blue: 36/255, alpha: 1)
        }
        
        self.layer.borderWidth = width
        self.layer.cornerRadius = radius
        
    }
    
    func setPadding()
    {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = UITextField.ViewMode.always
        
    }

}
