//
//  ViewExtension.swift
//  EVChargeProvider
//
//  Created by MURALI on 06/01/22.
//

import Foundation
import UIKit

extension UIView {
    
    enum Colour {
        case green
        case orange
        case black
    }
    
    func setViewRoundCornor() {
        self.layer.cornerRadius = self.frame.size.height/2
    }
    
    func setViewCornerWithRadiusAndNoBorder(radius: CGFloat) {
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }

    func setViewCornerRadiusWithBorder(radius: CGFloat, width: CGFloat, color: Colour) {
    
        
        switch color {
        case .green:
            self.layer.borderColor = CGColor(red:38/255, green: 217/255, blue: 117/255, alpha: 1)

        case .orange:
            self.layer.borderColor = CGColor(red:255/255, green: 105/255, blue: 36/255, alpha: 1)
            
        case .black:
            self.layer.borderColor = CGColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        }
        
        self.layer.borderWidth = width
        self.layer.cornerRadius = radius
    }
    
    func setViewCornorRadiusWithShadow(radius: CGFloat) {
        
        self.layer.cornerRadius = radius
        self.layer.shadowRadius = 5
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.layer.shadowOpacity = 1
    }
    
   
}


extension UIViewController {
    
    func pushViewController(controller: String) {
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let controller = self.storyboard!.instantiateViewController(withIdentifier: controller)
//        navigationController?.pushViewController(controller, animated: true)
        let vc = self.storyboard?.instantiateViewController(identifier: controller)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
