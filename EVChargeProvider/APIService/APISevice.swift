//
//  APIManager.swift
//  EVChargeProvider
//
//  Created by MURALI on 24/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import Foundation

class APIService {
    
    // Variable
    var urlRequest: URLRequest?
    
//    init(urlRequest: URLRequest) {
//        self.urlRequest = urlRequest
//    }
  
    func urlRequestConfiguration(endPoint: APIList, method: HTTPMethodList, parameter: [String: Any]?) {
        
        
        
        let url = baseURL + endPoint.rawValue
        
        urlRequest = URLRequest(url: URL(string: url)!)
        print("Sending Url  \(String(describing: urlRequest?.url))")
        
        urlRequest?.httpMethod = method.rawValue
        
        // Header
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest?.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        urlRequest?.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        
        if parameter == nil {
            print("Parameter is nil")
            
        } else {
            print("Parameter are available")
            var parmsData: Data?
           // let encode = RequestModel(user_name: "", email: "")
            do {
                //parmsData = try JSONEncoder().encode(encode)
            } catch {
                print("Encoding Error %%> \(error)")
            }
            
            urlRequest?.httpBody = parmsData
            
        }
    }
    
    
    
    // Get Request
    func getApiCall(endPoint: APIList?, url: String?, model: RequestModel?, completion: @escaping((String, String)->Void)) {
     
      
        urlRequestConfiguration(endPoint: endPoint!, method: .get, parameter: nil)
        
        URLSession.shared.dataTask(with: urlRequest!) { data, response, error in
            
            do {
                let json = try JSONDecoder().decode(RequestModel.self, from: data!)
                //let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any]
                print("Decodeed Output >> \(json)")
            } catch {
                
                print("ERROR:> \(error.localizedDescription)")
            }
        }.resume()
    }
    
    // Post
    func postApiCall(endPoint: APIList?, parameter: [String: Any], menthod: HTTPMethodList, completion: @escaping((String, Error)->Void)) {
        
        var selectedMethod: HTTPMethodList?
        
        var parmsData: Data?
       // let encode = RequestModel(user_name: "", email: "")
        do {
           // parmsData = try JSONEncoder().encode(encode)
            parmsData = try JSONSerialization.data(withJSONObject: parameter, options: [])
            
        } catch {
            print("Encoding Error %%> \(error)")
        }
        
        urlRequest?.httpBody = parmsData
        
        
        switch menthod {
        case .get:
            selectedMethod = .get
        case .post:
            selectedMethod = .post
        case .put:
            selectedMethod = .put
        case .patch:
            selectedMethod = .patch
        case .delete:
            selectedMethod = .delete
        }
        
        
        
        urlRequestConfiguration(endPoint: endPoint!, method: selectedMethod!, parameter: nil)
          
        print("Selecte Methdo \(urlRequest?.httpMethod)")
        
     
        
          URLSession.shared.dataTask(with: urlRequest!) { data, response, error in
              
              do {
                  let json = try JSONDecoder().decode(RequestModel.self, from: data!)
                  //let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any]
                  print("Decodeed Output >> \(json)")
              } catch {
                  
                  print("ERROR:> \(error.localizedDescription)")
              }
          }.resume()
    }
}
