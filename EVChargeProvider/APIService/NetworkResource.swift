//
//  APIList.swift
//  EVChargeProvider
//
//  Created by MURALI on 03/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import Foundation

var baseURL = "https://evzoneride.com/"

let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJjZmZhZmQ3YmFiMThiNDAyZGYwNWZlNDA1ZDE1OWVmNTMwNzcxYTFhN2Y0ZWUzOTE4NmRiN2NlY2UwYjE1NTNiZWM4NDIxYzgzODUwZDk2In0.eyJhdWQiOiIxIiwianRpIjoiMmNmZmFmZDdiYWIxOGI0MDJkZjA1ZmU0MDVkMTU5ZWY1MzA3NzFhMWE3ZjRlZTM5MTg2ZGI3Y2VjZTBiMTU1M2JlYzg0MjFjODM4NTBkOTYiLCJpYXQiOjE2NDU1NDQyNDEsIm5iZiI6MTY0NTU0NDI0MSwiZXhwIjoxOTYxMDc3MDQxLCJzdWIiOiIyMTIiLCJzY29wZXMiOltdfQ.dRcIikloj1Zmnum19fzQNGkzavTq2nlzO7fGo5xtcww84Rh3dY69cYYW01yrSOPxFr_iDajzPE6jBGRaYs8AVZgBFHZ00ZgVb-J-qBvL_QXz351-StOdFO-zTNjhWEebXoE8Oas8W1nGAz1z7i63DfJf8kBgWDttOOllxy0rezPQYBGohkrVgSVFKy9vSYxUKKIzL9VpR7TAADRlQStIIguB3LzGXd_f132Ir54Gb34qq79oVYRRw7Hqp9vLe7pWMronJg4_cVJKgajE9vgoMmOYcmF61COONZoqwoIrjypH8ZfKAEkCJR2x31wZYvKWnhbxalNCxmU_3nL2t1LBR1UuBiQwg3OaIwtP7i-vjAiAuDbnXG-WU9OLAEI2srksZDNeonXULu0hc2qQ7C0KxYHOc6OaWR_p_DBx7n3bfakTWQGvTI8Nn9dHkN8ktd8X8VD46tBmkaWlXg-5LgNKTKP_Y2xbhBGYGat7NpHFB2p2DkSJDD8T6Y6FUds0oDQP5IPlRLhp3w-9otP2Kla08QR1DfeUw0bdzvBlIOVdJ2XJn-xHmfMgc0vPaxTKeA17Yhj_Zk3QO1H5LKWbqitsU680vJUf46aWWKK4PdbeWtwjseWGezwPn48e4IJwkBEMFdomduqgu0sOnpbUMvrAn4FoWpf4KWpyPfuTBjAm10w"

enum APIList: String {
    case EV_PROFILE_GET = "api/user/sendotp"
    case profileApi = "api/provider/profile"
    case evprofile
    case profile_get = "api/user/details"
    case profile_post
    case estimatedApiList
    case farEstimatedApiList
}

enum HTTPMethodList: String {
    case get
    case post
    case put
    case patch
    case delete
}

enum ErrorList: String {
    case decodingError
    case uploadingError
    case getResponseError
}


enum SpinerAction {
    case start
    case stop
}
