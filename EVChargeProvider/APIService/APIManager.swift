//
//  APIService.swift
//  EVChargeProvider
//
//  Created by MURALI on 03/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

enum ChooseDecoder {
    case serialization
    case decoder
}

class APIManager {
    
    private var bodyData: Data?
    
    /// JSONSerialization, Error, JSONDecoder
    //var completion: ((Data?, String?)-> Void)?
    //var completion: ((String?, String?)-> Void)?
    var completion: ((RequestModel?, String?)-> Void)?
    
    // MARK: List of Api call
    func getApiCall(api: APIList, parameters: [String: Any]?) {
        checkingApiCall(api: api, method: .get, parameters: parameters, imageData: nil)
    }
    
    func postApiCall(api: APIList, httpMethod method: HTTPMethodList, parameters: [String: Any]) {
        checkingApiCall(api: api, method: method, parameters: parameters, imageData: nil)
    }
    func postApiCall(api: APIList, parameters: [String: Any], imageData: [String: Data]?) {
        checkingApiCall(api: api, method: .post, parameters: parameters, imageData: imageData)
    }
    
    // MARK: Checking imageData nil or not
    func checkingApiCall(api : APIList, method: HTTPMethodList, parameters: [String: Any]?, imageData: [String: Data]?) {
        
        if imageData != nil {
            
            uploadingRequest(api: api, parameters: parameters!, imageData: imageData)
            
        } else {
            
            sendRequest(api: api, method: method, parameters: parameters)
        }
    }
    
    
    func sendRequest(api apiUrl: APIList, method: HTTPMethodList, parameters: [String: Any]?) {
        
        var urlStr: URL?
        var urlRequest: URLRequest?
        var appendingUrl = ""
        
        switch method{
        case .get:
            
            if parameters == nil {
                
                appendingUrl = APIList.EV_PROFILE_GET.rawValue
                
            } else {
                
                for (index, param) in (parameters ?? [:]).enumerated() {
                    
                    appendingUrl.append("\(index == 0 ? "?" : "&")"+"\(param.key)=\(param.value)")
                }
                appendingUrl = apiUrl.rawValue+appendingUrl
                
            }
            
        case .post:
            
            appendingUrl = apiUrl.rawValue
            encodeGivenParameters()
            
        case .put, .patch, .delete:
            
            appendingUrl = apiUrl.rawValue
            encodeGivenParameters()
            
        }
        
        
        urlStr = URL(string: baseURL+appendingUrl)
        urlRequest = URLRequest(url: urlStr!)
        urlRequest?.httpMethod = method.rawValue
        
        // URLRequest Header
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest?.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        urlRequest?.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        
        // Convert ginven parameter to data
        func encodeGivenParameters() {
            
            do {
                bodyData = try JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
                urlRequest?.httpBody = bodyData
                print("Body data>> \(String(bytes: bodyData!, encoding: .utf8)?.description ?? "")")
            } catch {
                print("Given Parameters Encoding Error")
            }
        }
        
        AF.request(urlRequest!).responseJSON { response in
            
            switch response.result {
                
            case .failure(let error):
                
                print("Get Api Error >>+ \(error.localizedDescription)")
                
            case .success(let value):
                
                print(">>>>>>JSON Response successfully fetched<<<<<<< \n \(value)")
            }
            
            
            self.JSONDecoderResponse(responseData: response)
            //self.JSONSerializationResponse(responseData: response)
            
        }
        
    }
    
    private func uploadingRequest(api: APIList, parameters body: [String: Any], imageData: [String: Data]?) {
        
        guard let url = URL(string: baseURL+api.rawValue) else {
            print("Uploading Url Error >> \(baseURL+api.rawValue)")
            return
        }
        print("Uploading Url >> \(url)")
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(token)",
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest"
        ]
        
        //        if let auth = UserDefaults.standard.value(forKey: "auth_token") {
        //
        //            headers["Authorization"] = "Bearer \(token)"
        //        }
        
        AF.upload(multipartFormData: { uploadedData in
            
            for (key, value) in body {
                uploadedData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            
            if let imageData = imageData {
                
                for image in imageData {
                    uploadedData.append(image.value, withName: image.key, fileName: "doc.jpeg", mimeType: "image/jpeg")
                }
                
            }
            
            
        }, to: url, method: .post, headers: headers).responseJSON { response in
            
            
            print("Uploading Error >>++ \(String(describing: response.error))")
            self.JSONDecoderResponse(responseData: response)
        }
        
    }
    
    // MARK: Handling JSONResponse
    ///Convert AFDataResponse to json format
    private func JSONDecoderResponse(responseData: AFDataResponse<Any>) {
        
        do {
            //Handling response data
            if let data = responseData.data {
                let jsonDecoder = try JSONDecoder().decode(RequestModel.self, from: data)
              //  let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any]
                
               completion!(jsonDecoder,nil)
              //  completion!(json,nil)
                ///completion?(nil,nil, jsonDecoder)
                
            } else{
                // Handling response error
                let error = responseData.error?.underlyingError?.localizedDescription
                completion!(nil,error)
                // completion?(nil, responseData.error?.underlyingError?.localizedDescription ?? "", nil)
            }
            
        }
        catch {
            print("")
        }
    }
    
    private func JSONSerializationResponse(responseData: AFDataResponse<Any>) {
        
        do {
            //Handling response data
            if let data = responseData.data {
                
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [String: Any]
                
                //completion?(data,nil)
                //completion?(json,nil, nil)
                
            } else{
                // Handling response error
                //completion?(nil, responseData.error?.underlyingError?.localizedDescription ?? "", nil)
            }
            
        }
        catch {
            print("")
        }
    }
    
    // MARK: Show/Hide Activity Indicator
    func showSpinner(view: UIView, action: SpinerAction) {
        
        
        let subView = UIView()
        let loadingView = UIView()
        var spinner = UIActivityIndicatorView()
        
        switch action {
        case .start:
            
            subView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            subView.center = view.center
            subView.backgroundColor = UIColor(red: 236/255, green: 237/255, blue: 238/255, alpha: 0.1)
            
            
            loadingView.frame = CGRect(x: 0.0, y: 0.0, width: 80, height: 80)
            loadingView.center = view.center
            loadingView.setViewCornorRadiusWithShadow(radius: 10)
            loadingView.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            
            
            spinner = UIActivityIndicatorView(style: .large)
            spinner.color = UIColor(red: 38/255, green: 217/255, blue: 117/255, alpha: 1)
            spinner.center = CGPoint(x: loadingView.bounds.size.width/2, y: loadingView.bounds.size.height/2)
            
            loadingView.addSubview(spinner)
            subView.addSubview(loadingView)
            view.addSubview(subView)
            spinner.startAnimating()
            
        case .stop:
            
            spinner.stopAnimating()
            loadingView.removeFromSuperview()
            subView.removeFromSuperview()
            
        }
        
    }
    
}


