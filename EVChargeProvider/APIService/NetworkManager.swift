//
//  NetworkService.swift
//  EVChargeProvider
//
//  Created by MURALI on 28/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import Foundation
import UIKit

class NetworkManager {
    
    
    static func sendApiCall<T: Codable>(endPoint: APIList, method: HTTPMethodList, model: T.Type, parameter: [String: Any]?, completion: @escaping((T?, Error?)-> Void)) {
        
        var urlRequest: URLRequest?
        var url: String?
        var appendUrl: String = ""
        
        if parameter == nil {
             url = baseURL + endPoint.rawValue
        } else {
            for (index, parms) in parameter!.enumerated() {
                appendUrl.append((index == 0 ? "?": "&")+"\(parms.key)=\(parms.value)")
            }
            url =  baseURL + endPoint.rawValue+appendUrl
        }
        
        
        urlRequest = URLRequest(url: URL(string: url!)!)
        urlRequest?.httpMethod = method.rawValue
        
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest?.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        urlRequest?.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        print(urlRequest?.url!)

        URLSession.shared.dataTask(with: urlRequest!) { data, response, error in
            guard let data = data else {
                completion(nil,error)
                return
            }

            do {
                let json = try JSONDecoder().decode(model.self, from: data)
                let jsons = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                print(":: SEND API RESPONSE :: \n \(jsons ?? [:])")
                completion(json, nil)
                
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
        
    }
    
    
    static func postApiCall<T: Codable>(endPoint: APIList, model: T.Type, parameter: [String: Any], completion: @escaping((T?, Error?)-> Void)) {
        var urlRequest: URLRequest?
        let mainUrl = URL(string: baseURL + endPoint.rawValue)
        
        urlRequest = URLRequest(url: mainUrl!)
                
        urlRequest?.httpMethod = "POST"
        
        
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest?.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        urlRequest?.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        

        urlRequest?.httpBody = try? JSONSerialization.data(withJSONObject: parameter, options: [])
        
        print(urlRequest?.url)

        
        URLSession.shared.dataTask(with: urlRequest!) { data, response, error in
            guard let data = data else {
                completion(nil,error)
                return
            }

            do {
                let json = try JSONDecoder().decode(model.self, from: data)
                let jsons = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                print(":: POST API RESPONSE :: \n \(jsons ?? [:])")
                completion(json, nil)
                
            } catch {
                print("Error decoding")
            }
        }.resume()
        
    }
    
    
    static func post1ApiCall<T: Codable>(endPoint: APIList, model: T.Type, parameter: [String: Any], upload: [String: Data]?, completion: @escaping((T?, Error?)-> Void)) {
        var urlRequest: URLRequest?
        
        let mainUrl = URL(string: baseURL + endPoint.rawValue)
        urlRequest = URLRequest(url: mainUrl!)
        urlRequest?.httpMethod = "POST"
        

        urlRequest?.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest?.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest?.addValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
        urlRequest?.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        

        urlRequest?.httpBody = try? JSONSerialization.data(withJSONObject: parameter, options: [])
        
        print(urlRequest?.url)

        if upload == nil {
        
        URLSession.shared.dataTask(with: urlRequest!) { data, response, error in
            guard let data = data else {
                completion(nil,error)
                return
            }

            do {
                let json = try JSONDecoder().decode(model.self, from: data)
                let jsons = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                print(":: POST API RESPONSE :: \n \(jsons ?? [:])")
                completion(json, nil)
                
            } catch {
                print("Error decoding")
            }
        }.resume()

        } else {
        
        URLSession.shared.dataTask(with: urlRequest!) { data, response, error in
            guard let data = data else {
                completion(nil,error)
                return
            }

            do {
                let json = try JSONDecoder().decode(model.self, from: data)
                let jsons = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]
                print(":: POST API RESPONSE :: \n \(jsons ?? [:])")
                completion(json, nil)
                
            } catch {
                print("Error decoding")
            }
        }.resume()
            
        }

    }
    
    func imageUpload<T: Codable>(endPoint: APIList, imageData: [String: Data]?, model: T.Type, completion: @escaping(T, Error)-> Void) {
        
        var urlRequest: URLRequest?
        let lineBreak = "\r\n"
        
        let mainUrl = URL(string: baseURL + endPoint.rawValue)
        urlRequest = URLRequest(url: mainUrl!)
        urlRequest?.httpMethod = "POST"
        
        let boundary = "-------------------\(UUID().uuidString)"
        urlRequest?.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "content-type")
        
        var requestData = Data()
        requestData.append("--\(boundary)\r\n".data(using: .utf8)!)
        requestData.append("content-disposition: form-data; name=\"attachment\"\(lineBreak + lineBreak)".data(using: .utf8)!)
        
        for parms in imageData! {
            requestData.append(parms.key.data(using: .utf8)!)
        }
        
        requestData.append("--\(boundary)\r\n".data(using: .utf8)!)
        requestData.append("content-disposition: form-data; name=\"fileName\"\(lineBreak + lineBreak)".data(using: .utf8)!)
     
        
        for parms in imageData! {
           // requestData.append(parms.key.data(using: .utf8)!)
            requestData.append("\(parms.key + lineBreak)".data(using: .utf8)!)
        }
        requestData.append("--\(boundary)--\(lineBreak)".data(using: .utf8)!)
        
        urlRequest?.addValue("\(requestData.count)", forHTTPHeaderField: "content-length")
        urlRequest?.httpBody = requestData
        
        
        
        
    }
    
//    func uploadImage(url: String, imageData: [String: Any]?){
//        // the image in UIImage type
//        //guard let image = tmpImage else { return  }
//
//        let filename = "avatar.png"
//
//        // generate boundary string using a unique per-app string
//        let boundary = UUID().uuidString
//
//        let fieldName = "reqtype"
//        let fieldValue = "fileupload"
//
//        let fieldName2 = "userhash"
//        let fieldValue2 = "caa3dce4fcb36cfdf9258ad9c"
//
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//
//        // Set the URLRequest to POST and to the specified URL
//        var urlRequest = URLRequest(url: URL(string: "https://catbox.moe/user/api.php")!)
//        urlRequest.httpMethod = "POST"
//
//        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
//        // And the boundary is also set here
//        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//
//        var data = Data()
//
//        // Add the reqtype field and its value to the raw http request data
//        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
//        data.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
//        data.append("\(fieldValue)".data(using: .utf8)!)
//
//        // Add the userhash field and its value to the raw http reqyest data
//        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
//        data.append("Content-Disposition: form-data; name=\"\(fieldName2)\"\r\n\r\n".data(using: .utf8)!)
//        data.append("\(fieldValue2)".data(using: .utf8)!)
//
//        // Add the image data to the raw http request data
//        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
//        data.append("Content-Disposition: form-data; name=\"fileToUpload\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
//        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
//        data.append(UIImagePNGRepresentation(image)!)
//
//        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
//        // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
//        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
//
//        // Send a POST request to the URL, with the data we created earlier
//        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
//            
//            if(error != nil){
//                print("\(error!.localizedDescription)")
//            }
//            
//            guard let responseData = responseData else {
//                print("no response data")
//                return
//            }
//            
//            if let responseString = String(data: responseData, encoding: .utf8) {
//                print("uploaded to: \(responseString)")
//            }
//        }).resume()
//
//    }
    
}
