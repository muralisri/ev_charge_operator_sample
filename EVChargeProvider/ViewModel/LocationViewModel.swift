//
//  LocationViewModel.swift
//  EVChargeProvider
//
//  Created by MURALI on 28/01/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import Foundation
import GooglePlaces
import GoogleMaps


let defaultLocation = CLLocationCoordinate2D(latitude: 16.5214, longitude: 80.6008)
class LocationViewModel: NSObject {
    
    // Variables
    private var currentLocation : ((CLLocation)->Void)?
    var locationManager = CLLocationManager()
    var geocoder = CLGeocoder()
    
    
    func setGoogleMap(mapView: GMSMapView) {
 
        let camera = GMSCameraPosition(latitude: defaultLocation.latitude, longitude: defaultLocation.longitude, zoom: 12)
        mapView.animate(to: camera)
        mapView.isMyLocationEnabled = true
        
        print("GetGoogleMap **>")
        
    }

    func getCurrenctLocation(onUpdatedLocation: @escaping ((CLLocation)-> Void)) {
        
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        locationManager.allowsBackgroundLocationUpdates = false // true
        
        //locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        self.currentLocation = onUpdatedLocation
   
    }
    
    

}

extension LocationViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("updatelocation **>\(locations.last!)")
        
        guard let location = locations.last else {
            return
        }
        
        self.locationManager.stopUpdatingLocation()
        self.currentLocation?(location)
        
    
        //ReverseGeocoder
        geocoder.reverseGeocodeLocation(location) { placeMark, error in
            
            print("Reverse Geocoder location**> \(placeMark ?? [])")
            print("Reverse Geocoder location Error**> \(error?.localizedDescription ?? "")")
            
            guard let placeMark = placeMark?.last else {
                return
            }
            print("Full Address ++>> \(placeMark.name ?? ""), \(placeMark.locality ?? ""), \(placeMark.administrativeArea ?? ""), \(placeMark.postalCode ?? "")")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Error **> \(error)")
    }
    
}
