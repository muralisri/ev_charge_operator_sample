//
//  SideMenuTableViewCell.swift
//  EVChargeProvider
//
//  Created by MURALI on 09/01/22.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuNameLabel: UILabel!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var bottomLineLable: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }

}
