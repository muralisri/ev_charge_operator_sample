//
//  SideMenuViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 08/01/22.
//

import UIKit
import SideMenu

class SideMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
 
    @IBOutlet weak var menuTableView: UITableView!
    
    
    var menuList = ["Account",
                    "Mobile Charge Point",
                    "Notifications",
                    "Scheduled Mobile Charge",
                    "Transactions",
                    "Wallet",
                    "Privacy Policy",
                    "Terms of Use",
                    "Contact Us",
                    "Settings",
                    "Sign Out"]
    
    var menuListImage = ["account",
                         "mobile_charge_point",
                         "notifications",
                         "schduled_mobile_charge",
                         "transactions",
                         "wallet",
                         "privacy_policy",
                         "terms_use",
                         "message",
                         "setting",
                         "sign_out"]
    
    // Fixed Charge Point
    var fixedMenuList = ["Account",
                         "Notifications",
                    "Scheduled Fixed Charging",
                    "Fixed Charging Transactions",
                    "Wallet",
                    "Privacy Policy",
                    "Terms of Use",
                    "Contact Us",
                    "Settings",
                    "Sign Out"]
    
    var fixedMenuListImage = ["account",
                         "notifications",
                         "sheduled-fixed-charging",
                         "fixed-charging-transaction",
                         "wallet",
                         "privacy_policy",
                         "terms_use",
                         "message",
                         "setting",
                         "sign_out"]
    
    var operatorType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
        
        navigationController?.isNavigationBarHidden = true
        
        menuTableView.dataSource = self
        menuTableView.delegate = self
        menuTableView.separatorStyle = .none
        menuTableView.backgroundColor = .white
    
    }
    
    func initialLoad() {
        
        //operatorType = UserDefaults.standard.value(forKey: "operator_type") as! Int
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if operatorType == 0 {
            return menuList.count
            
        } else {
            
            return fixedMenuList.count
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as? SideMenuTableViewCell
        
        if operatorType == 0 {
            
            cell?.menuNameLabel.text = menuList[indexPath.row]
            cell?.menuImage.image = UIImage(named: menuListImage[indexPath.row])
            
            if indexPath.row == 5 {
                cell?.bottomLineLable.backgroundColor = .lightGray
            }
            
            return cell!
            
        } else {
            cell?.menuNameLabel.text = fixedMenuList[indexPath.row]
            cell?.menuImage.image = UIImage(named: fixedMenuListImage[indexPath.row])
            
            if indexPath.row == 4 {
                cell?.bottomLineLable.backgroundColor = .lightGray
            }
            return cell!
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let index = indexPath.row
        
        if operatorType == 0 {
        
        switch index {
            
        case 0:
            push(controller: "MyAccountViewController", animated: true)       
        case 1:
            push(controller: "MobileChargePointInfoViewController", animated: true)
        case 2:
            push(controller: "NotificationViewController", animated: true)
        case 3:
            push(controller: "ScheduleMobileChargingViewController", animated: true)
        case 4:
            push(controller: "MobileChargingTransactionsViewController", animated: true)
        case 5:
            push(controller: "WalletViewController", animated: true)
        case 6:
            //push(controller: "", animated: true)
            return
        case 7:
            //push(controller: "", animated: true)
            return
        case 8:
            //push(controller: "ChargingInfoViewController", animated: true)
            return
        case 9:
            push(controller: "SettingsViewController", animated: true)
        case 10:
            //push(controller: "", animated: true)
            return
        default:
            return
        }
        } else {
            
            switch index {
                
            case 0:
                push(controller: "MyAccountViewController", animated: true)
            case 1:
                push(controller: "NotificationViewController", animated: true)
            case 2:
                push(controller: "ScheduledFixedChargingViewController", animated: true)
            case 3:
                push(controller: "FixedChargeingTranscationsViewController", animated: true)
            case 4:
                push(controller: "WalletViewController", animated: true)
            case 5:
                return
            case 6:
                //push(controller: "", animated: true)
                return
            case 7:
                //push(controller: "", animated: true)
                return
            case 8:
                push(controller: "SettingsViewController", animated: true)
            case 9:
                
                return
            default:
                return
            }
            
        }
      
    }
    


}
