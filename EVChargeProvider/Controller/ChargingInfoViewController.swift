//
//  ChargingInfoViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 10/01/22.
//

import UIKit
import FLAnimatedImage

class ChargingInfoViewController: UIViewController {
//Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var timePresetView: UIView!
    @IBOutlet weak var kWhUsedView: UIView!
    @IBOutlet weak var estRangeAddedView: UIView!
  
    @IBOutlet weak var loadingImage: FLAnimatedImageView!
    
    @IBOutlet weak var chargingLable: UILabel!
    @IBOutlet weak var timePreset: UILabel!
    @IBOutlet weak var kWhUsed: UILabel!
    @IBOutlet weak var estRangeAdded: UILabel!
    
    @IBOutlet weak var endChargeButton: UIButton!
    
    
    var chargingReceiptView: ChargingReceiptView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDesignStyle()
        initialLoad()
        setLodingGifAnimation()
    }
    func initialLoad() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        endChargeButton.addTarget(self, action: #selector(endChargeButtonAction), for: .touchUpInside)
        
    }
    
    func setDesignStyle() {
        
        topView.setViewCornerWithRadiusAndNoBorder(radius: 30)
        bottomView.setViewCornerWithRadiusAndNoBorder(radius: 30)
        
        timePresetView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        kWhUsedView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        estRangeAddedView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        endChargeButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        
    }
    func setLodingGifAnimation() {
        let path = Bundle.main.path(forResource: "Loading", ofType: "gif")
        let url = URL(fileURLWithPath: path!)
        let gifData = try? Data(contentsOf: url)
        let imageData = FLAnimatedImage(animatedGIFData: gifData)
        loadingImage.animatedImage = imageData
        
    }
    
    @objc
    func backButtonAction() {
        dismiss(animated: true)
    }
    
    @objc func endChargeButtonAction() {
        loadChargingReceiptView()
        
    }
    
    
    func loadChargingReceiptView() {
        
        if chargingReceiptView == nil, let bundle = Bundle.main.loadNibNamed("ChargingReceiptView", owner: self, options: nil)?.first as? ChargingReceiptView {
            
            self.chargingReceiptView = bundle
            
            self.chargingReceiptView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
            self.chargingReceiptView?.backButton.addTarget(self, action: #selector(receiptBackButtonAction), for: .touchUpInside)
            self.chargingReceiptView?.exportButton.addTarget(self, action: #selector(exportButtonAction), for: .touchUpInside)
            
            self.view.addSubview(self.chargingReceiptView!)
        }
    }
    
    func removeChargingReceiptView() {
        self.chargingReceiptView?.removeFromSuperview()
        self.chargingReceiptView = nil
    }
    
    @objc func receiptBackButtonAction() {
        removeChargingReceiptView()
    }
    
    @objc func exportButtonAction() {
        push(controller: "HomeViewController", animated: true)
    }

}
