//
//  ViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 06/01/22.
//

import UIKit

class InitialViewController: UIViewController {

    // UIView
    @IBOutlet weak var backgroundImgView: UIView!
    
    // UIButton
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var homeViewModel = HomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialLoad()
        setDesignStyle()
        
        homeViewModel.getResult()
    }

    func initialLoad() {
        
        signInButton.addTarget(self, action: #selector(signInButtonAction), for: .touchUpInside)
        signUpButton.addTarget(self, action: #selector(signUpButtonAction), for: .touchUpInside)
        
    }
    
    @objc func signInButtonAction() {
        
        push(controller: "SignInViewController", animated: true)
    }
    
    @objc func signUpButtonAction() {
        
        push(controller: "SignUpViewController", animated: true)
    }
    func setDesignStyle() {
    
        // Button
        signInButton.setButtonCornorRadiusWithGreenBorder(radius: 7, width: 3)
        signUpButton.setButtonCornorRadiusWithGreenBorder(radius: 7, width: 3)
        
        languageButton.setButtonCornorRadiusWithGreenBorder(radius: 7, width: 0)
        
        
    }

}

