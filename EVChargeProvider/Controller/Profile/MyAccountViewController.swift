//
//  MyAccountViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import UIKit

class MyAccountViewController: UIViewController {

    // Navigation
     @IBOutlet weak var navigationTitle: UILabel!
     @IBOutlet weak var backButton: UIButton!
     
     // Profile
     @IBOutlet weak var profileImage: UIImageView!
     @IBOutlet weak var cameraButton: UIButton!
     @IBOutlet weak var cameraOuterView: UIView!
     @IBOutlet weak var cameraInnerView: UIView!
     
     // Driver License
     @IBOutlet weak var driverLicenseLabel: UILabel!
     @IBOutlet weak var fileDragButton: UIButton!
     
     // Details
     @IBOutlet weak var firstName: UITextField!
     @IBOutlet weak var lastName: UITextField!
     @IBOutlet weak var address: UITextField!
     @IBOutlet weak var emailAddress: UITextField!
     @IBOutlet weak var country: UITextField!
     @IBOutlet weak var phoneNumber: UITextField!
     @IBOutlet weak var password: UITextField!
     
     @IBOutlet weak var signUpButton: UIButton!
     @IBOutlet weak var countryButton: UIButton!
     @IBOutlet weak var eyeButton: UIButton!
     
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var phoneNumberView: UIView!
    
    @IBOutlet weak var countryView: UIView!
    
    override func viewDidLoad() {
         super.viewDidLoad()
         
         initialLoad()
         setDesignStyle()
         
     }
    
    func initialLoad() {
        
    backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
    
    @objc func backButtonAction() {
        
        dismiss(animated: true)
    }
     
     func setDesignStyle() {
         profileImage.setImageCornorRoundWithGreenBorder(width: 2)
         cameraOuterView.setViewRoundCornor()
         cameraInnerView.setViewRoundCornor()
         
         fileDragButton.setButtonCornorRadiusWithGreenBorder(radius: 7, width: 2)
         signUpButton.setButtonCornorRadiusWithGreenBorder(radius: 5, width: 0)
         
         // TextField
         firstName.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
         lastName.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
         address.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
         emailAddress.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
         
         countryView.setViewCornerRadiusWithBorder(radius: 7, width: 1, color: .green)
         phoneNumberView.setViewCornerRadiusWithBorder(radius: 7, width: 1, color: .green)
         passwordView.setViewCornerRadiusWithBorder(radius: 7, width: 1, color: .green)

         
     } 

}
