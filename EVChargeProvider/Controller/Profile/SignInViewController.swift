//
//  LoginViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 06/01/22.
//

import UIKit

class SignInViewController: UIViewController {

    // UIView
    @IBOutlet weak var backgroundImgView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    //UIButton
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var passwordEyeButton: UIButton!
    
    // UITextField
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    // UILabel
    @IBOutlet weak var navigationTitle: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    // Variable
    var operatorType: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()
        onClickButtonAction()
    }
    
    func setDesignStyle() {
    
        // TextField
        emailTextField.setTextFieldCornorRadiusWithBorder(radius: 7, width: 3, color: .green)
        passwordView.setViewCornerRadiusWithBorder(radius: 7, width: 3, color: .green)
        
        emailTextField.setPadding()
        passwordTextField.setPadding()
        
        //Button
        signInButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
    }
    
    func initialLoad() {
        operatorType = UserDefaults.standard.value(forKey: "operator_type") as? Int
       
    }
    
    func onClickButtonAction() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        signInButton.addTarget(self, action: #selector(signInButtonAction), for: .touchUpInside)
    }
    
    @objc func backButtonAction() {

        dismiss(animated: true)
    }

    @objc func signInButtonAction() {
        
        print("Selected Operator = \(operatorType)")
        
        if operatorType == nil {
            showAlert(message: "Signup first")
            
        } else {
        
        if operatorType == 0 {
            push(controller: "HomeViewController", animated: true)
            
        } else {
            push(controller: "ChargePointViewController", animated: true)
        }
        
        }
    }

}
