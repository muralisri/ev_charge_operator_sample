//
//  QrCodeScannerViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 20/01/22.
//

import UIKit

class QrCodeScannerViewController: UIViewController {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // balance
    @IBOutlet weak var availableBalance: UILabel!
    @IBOutlet weak var availableAmount: UILabel!
    @IBOutlet weak var currencyButton: UIButton!
    @IBOutlet weak var balanceHoldingView: UIView!
    
    // QRCode Scanner
    @IBOutlet weak var scannerHoldingView: UIView!
    @IBOutlet weak var connectNow: UILabel!
    @IBOutlet weak var scannerDescription: UILabel!
    @IBOutlet weak var scannerView: UIView!
    @IBOutlet weak var readerNotWorkingButton: UIButton!
    @IBOutlet weak var enterPointIdButton: UIButton!
    @IBOutlet weak var currencyContainerView: UIView!
    @IBOutlet weak var currencyViewBottomConstrain: NSLayoutConstraint!
    
    // XIB View
    var qrCodeScannerConfirmView: QrCodeScannerConfirmView?
    var qrCodeScannerEnterPointIdView: QrCodeScannerEnterPointIdView?
    var startEvChargingOTPView: StartEvChargingOTPView?
    var dropDownMenuView: DropDownMenuView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDesignStyle()
        initialLoad()
        onClickButtonAction()

        // For MenuButton First action
        if #available(iOS 14.0, *) {
            currencyButtonAction()
        } else {
            //
        }
   
    }
    
    func initialLoad() {
        self.currencyViewBottomConstrain.constant = currencyContainerView.frame.height
    }

    func setDesignStyle() {
        
        balanceHoldingView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        scannerHoldingView.setViewCornerWithRadiusAndNoBorder(radius: 25)
        connectNow.setLableSomeCornerRadius(radius: 25)
        
        enterPointIdButton.setViewCornerWithRadiusAndNoBorder(radius: 5)
        
        currencyContainerView.setViewCornerWithRadiusAndNoBorder(radius: 10)
    }
    
    func onClickButtonAction() {
        
        enterPointIdButton.addTarget(self, action: #selector(enterPointIdButtonAction), for: .touchUpInside)
        readerNotWorkingButton.addTarget(self, action: #selector(readerNotWorkingButtonAction), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        currencyButton.addTarget(self, action: #selector(currencyButtonAction), for: .touchUpInside)
        
        // Tap Gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapViewAction(_:)))
        scannerView.addGestureRecognizer(tap)
        scannerHoldingView.addGestureRecognizer(tap)
    }
    
      @objc func tapViewAction(_ sender: UITapGestureRecognizer) {
          dismissDropDownMenu()
        }
    
    @objc func enterPointIdButtonAction() {
        dismissDropDownMenu()
        loadQrCodeScannerEnterPointIdView()
    }
    
    @objc func readerNotWorkingButtonAction() {
        dismissDropDownMenu()
        //loadQrCodeScannerConfirmView()
    }
    
    @objc func backButtonAction() {
        dismissDropDownMenu()
        dismiss(animated: true)
    }
    
    @objc func currencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: currencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            UIView.animate(withDuration: 0.2) {
                self.loadDropDownMenuView()
                self.currencyViewBottomConstrain.constant = 30
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
    @objc func confirmButtonAction() {
        removeQrCodeScannerConfirmView()
        push(controller: "RequiredChargeViewController", animated: true)
    }
    
    @objc func submitButtonAction() {
        
        dismissDropDownMenu()
        removeQrCodeScannerEnterPointIdView()
        loadQrCodeScannerConfirmView()
        
    }
}

// MARK: Add UIView
extension QrCodeScannerViewController {
    
    func loadDropDownMenuView() {
        
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
       
            self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.currencyContainerView.frame.width, height: self.currencyContainerView.frame.height)
            self.dropDownMenuView?.selectedValue = { name, status in
                self.currencyButton.setTitle(name, for: .normal)
                
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            
            self.currencyContainerView.addSubview(self.dropDownMenuView!)
        }
    }
    
    func dismissDropDownMenu() {
        UIView.animate(withDuration: 0.2) {
            
            self.currencyViewBottomConstrain.constant = self.currencyContainerView.frame.height
            self.view.layoutIfNeeded()
            self.dropDownMenuView?.titleView.isHidden = true
            
        } completion: { _ in
            self.removeDropDownMenuView()
            self.dropDownMenuView?.titleView.isHidden = false
        }
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
    
    func loadQrCodeScannerConfirmView() {
        
        if qrCodeScannerConfirmView == nil, let bundle = Bundle.main.loadNibNamed("QrCodeScannerConfirmView", owner: self, options: nil)?.first as? QrCodeScannerConfirmView {
            
            self.qrCodeScannerConfirmView = bundle
            self.qrCodeScannerConfirmView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
            self.qrCodeScannerConfirmView?.confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
            
            self.view.addSubview(self.qrCodeScannerConfirmView!)
        }
    }
  
    func removeQrCodeScannerConfirmView() {
        qrCodeScannerConfirmView?.removeFromSuperview()
        qrCodeScannerConfirmView = nil
        
    }
    
    func loadQrCodeScannerEnterPointIdView() {
        
        if qrCodeScannerEnterPointIdView == nil, let bundle = Bundle.main.loadNibNamed("QrCodeScannerEnterPointIdView", owner: self, options: nil)?.first as? QrCodeScannerEnterPointIdView {
            
            self.qrCodeScannerEnterPointIdView = bundle
            self.qrCodeScannerEnterPointIdView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
            self.qrCodeScannerEnterPointIdView?.submitButton.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
            
            self.view.addSubview(self.qrCodeScannerEnterPointIdView!)
        }
    }
    
    func removeQrCodeScannerEnterPointIdView() {
        
        qrCodeScannerEnterPointIdView?.removeFromSuperview()
        qrCodeScannerEnterPointIdView = nil
    }
}

