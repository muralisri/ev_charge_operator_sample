//
//  SettingsViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import UIKit

class SettingsViewController: UIViewController {

    // Navigation
    
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // Profile
    @IBOutlet weak var profileHoldingView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var editAccountButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
        setDesignStyle()
        
    }
    
    
    func initialLoad() {
        
        
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
    
    @objc func backButtonAction() {
        
        dismiss(animated: true)
    }
        
  
    func setDesignStyle() {
       profileHoldingView.setViewCornerWithRadiusAndNoBorder(radius: 40)
       profileView.setViewRoundCornor()
    }

}
