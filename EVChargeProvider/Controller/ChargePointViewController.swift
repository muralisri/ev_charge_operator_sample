//
//  ChargePointViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 23/01/22.
//

import UIKit
import SideMenu

class ChargePointViewController: UIViewController {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    
    // ChargePointDetails
    
    @IBOutlet weak var chargePointBackgroundImage: UIImageView!
    @IBOutlet weak var chargePointImage: UIImageView!
    @IBOutlet weak var chargePointName: UILabel!
    @IBOutlet weak var chargePointAddress: UILabel!
    
    // Details
    @IBOutlet weak var chargePointDetailsView: UIView!
//    @IBOutlet weak var distanceLabel: UILabel!
//    @IBOutlet weak var distance: UILabel!
//    @IBOutlet weak var durationLabel: UILabel!
//    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var cost: UILabel!
    
    @IBOutlet weak var chargePointView: UIView!
    @IBOutlet weak var maxPower1Image: UIImageView!
    @IBOutlet weak var maxPower2Image: UIImageView!
    @IBOutlet weak var maxPower1Label: UILabel!
    @IBOutlet weak var maxPowe2Label: UILabel!
    
    
    // How to charge
    @IBOutlet weak var howToCharge: UILabel!
    @IBOutlet weak var scanQR: UILabel!
    @IBOutlet weak var selectPayment: UILabel!
    @IBOutlet weak var plugIn: UILabel!
    @IBOutlet weak var selectRequired: UILabel!
    @IBOutlet weak var startCharge: UILabel!
    
    // User Details
    @IBOutlet weak var userDetailView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var licensePlateNoLabel: UILabel!
    @IBOutlet weak var licensePlateNo: UITextField!
    @IBOutlet weak var scanQRCodeButton: UIButton!
    
    //
    var menu: SideMenuNavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()
        setSideMenuConteoller()
  
    }
    
    func setSideMenuConteoller() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sideMenuController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
        menu = SideMenuNavigationController(rootViewController: sideMenuController!)
        menu?.leftSide = true
        menu?.menuWidth = self.view.frame.width/1.3
        
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.rightMenuNavigationController = nil
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
    }
    

    func initialLoad() {
        menuButton.addTarget(self, action: #selector(menuButtonAction), for: .touchUpInside)
        
        scanQRCodeButton.addTarget(self, action: #selector(scanQRCodeButtonAction), for: .touchUpInside)
        
    }
    
    func setDesignStyle() {
        
        chargePointDetailsView.setViewCornorRadiusWithShadow(radius: 20)
        chargePointView.setViewCornerWithRadiusAndNoBorder(radius: 10)
        userDetailView.setViewCornorRadiusWithShadow(radius: 20)
        
        userName.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
        licensePlateNo.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
        scanQRCodeButton.setButtonRoundCornerWithShadow(style: .normal, radius: 5)
        chargePointImage.setImageCornorRadius(radius: 7)
    }
    
    @objc func menuButtonAction() {
        present(menu!, animated: true)
        
    }
    
    @objc func scanQRCodeButtonAction() {
        push(controller: "QrCodeScannerViewController", animated: true)
    }
    
   
}
