//
//  NotificationViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 13/01/22.
//

import UIKit

class NotificationViewController: UIViewController {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var notificationsCount: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()
    
       
    }
    
    func initialLoad() {
        
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
    
    func setDesignStyle() {
        notificationsCount.setLabelCornerRadius(radius: 10)
       
    }
    
    @objc func backButtonAction() {
        dismiss(animated: true)
    }
    
    
}
