//
//  PaymentMethodViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 21/01/22.
//

import UIKit

class PaymentMethodViewController: UIViewController {

    //Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // Account Balance
    @IBOutlet weak var AccountBalance: UILabel!
    @IBOutlet weak var balanceAmount: UILabel!
    @IBOutlet weak var accountCurrencyButton: UIButton!
    @IBOutlet weak var balanceView: UIView!
    
    @IBOutlet weak var addCardButton: UIButton!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var payCurrencyButton: UIButton!
    @IBOutlet weak var payAmountView: UIView!
    
    @IBOutlet weak var cardColletionView: UICollectionView!
    @IBOutlet weak var payMethodColletionView: UICollectionView!
    
    let payMethodImage = ["walletMoney", "flutterWay", "applePay", "gpay", "paypal",]
    let payMethodImageWhite = ["walletMoneyWhite", "flutterWay", "applePayWhite", "gpayWhite", "paypalWhite"]
    
    var selectedIndex: IndexPath?
    
    let accountCurrencyView = UIView()
    let payCurrencyView = UIView()
    var accountCurrencyHeightAnchor: NSLayoutConstraint?
    var payCurrencyHeightAnchor: NSLayoutConstraint?
    
    var tappedCurrency = true
    
    // XIB
    var dropDownMenuView: DropDownMenuView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDesignStyle()
        initialLoad()
        onClickButtonAction()

        cardColletionView.delegate = self
        cardColletionView.dataSource = self
        
        payMethodColletionView.delegate = self
        payMethodColletionView.dataSource = self
        
        // For MenuButton First action
        if #available(iOS 14.0, *) {
            accountCurrencyButtonAction()
            payCurrencyButtonAction()
        } else {
            //
        }

  
    }
    
    func initialLoad() {
        
        let nib = UINib(nibName: "AddCardCollectionViewCell", bundle: nil)
        cardColletionView.register(nib, forCellWithReuseIdentifier: "AddCardCell")
        
        let nib1 = UINib(nibName: "PaymentMethodCollectionViewCell", bundle: nil)
        payMethodColletionView.register(nib1, forCellWithReuseIdentifier: "PayModeCell")
        
        // For Currency DropDown
        accountCurrencyView.translatesAutoresizingMaskIntoConstraints = false
        accountCurrencyView.backgroundColor = .gray
        view.addSubview(accountCurrencyView)
        
        NSLayoutConstraint.activate([
            accountCurrencyView.topAnchor.constraint(equalTo: balanceView.bottomAnchor, constant: 0),
            accountCurrencyView.widthAnchor.constraint(equalToConstant: 200),
            accountCurrencyView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
            
        ])
        
        // For Currency DropDown
        payCurrencyView.translatesAutoresizingMaskIntoConstraints = false
        payCurrencyView.backgroundColor = .gray
        view.addSubview(payCurrencyView)
        
        NSLayoutConstraint.activate([
            payCurrencyView.bottomAnchor.constraint(equalTo: payAmountView.topAnchor, constant: 0),
            payCurrencyView.widthAnchor.constraint(equalToConstant: 200),
            payCurrencyView.trailingAnchor.constraint(equalTo: view.centerXAnchor, constant: 20)
            
        ])
  
    }
    
    func onClickButtonAction() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        addCardButton.addTarget(self, action: #selector(addCardButtonAction), for: .touchUpInside)
        payNowButton.addTarget(self, action: #selector(payNowButtonAction), for: .touchUpInside)
        accountCurrencyButton.addTarget(self, action: #selector(accountCurrencyButtonAction), for: .touchUpInside)
        payCurrencyButton.addTarget(self, action: #selector(payCurrencyButtonAction), for: .touchUpInside)
        
        
        
        
    }
    
    func setDesignStyle() {
        
        balanceView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        payNowButton.setButtonRoundCornerWithShadow(style: .normal, radius: 7)
        payAmountView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        payCurrencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
        accountCurrencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
    }
    
    @objc func backButtonAction() {
        dismiss(animated: true)
    }
    
    @objc func addCardButtonAction() {
        
        push(controller: "AddCardViewController", animated: true)
    }
    
    @objc func payNowButtonAction() {
        push(controller: "OtpVerifyViewController", animated: true)
    }
    
    @objc func accountCurrencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: accountCurrencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            tappedCurrency = true
            UIView.animate(withDuration: 0.2) {
                self.accountCurrencyHeightAnchor = self.accountCurrencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.7)
                self.accountCurrencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()
            }
        }
        
    }
    
    @objc func payCurrencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: payCurrencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            tappedCurrency = false
            UIView.animate(withDuration: 0.2) {
                self.payCurrencyHeightAnchor = self.payCurrencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.7)
                self.payCurrencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()
            }
        }
        
        
    }


}

extension PaymentMethodViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == cardColletionView {
            return 2
        } else {
            return payMethodImage.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
        if collectionView == cardColletionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCardCell", for: indexPath) as? AddCardCollectionViewCell
                
       
            cell?.addCardCellView.setViewCornerWithRadiusAndNoBorder(radius: 10)
                
                return cell!
                
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PayModeCell", for: indexPath) as? PaymentMethodCollectionViewCell
                
                cell?.payModeImage.image = UIImage(named: payMethodImage[indexPath.row])
             
              
                let orangeColor = UIColor(red: 255/255, green: 105/255, blue: 36/255, alpha: 1)
                let backgroundView = UIView()
                        backgroundView.backgroundColor = orangeColor
                cell?.selectedBackgroundView = backgroundView
                
                cell?.selectedBackgroundView?.layer.cornerRadius = 20
                cell?.selectedBackgroundView?.layer.masksToBounds = true
                    
                    return cell!
            }
                
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let selectedCell = collectionView.cellForItem(at: indexPath) as? PaymentMethodCollectionViewCell
        
        selectedCell?.payModeImage.image = UIImage(named: payMethodImageWhite[indexPath.row])
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
//        let index = indexPath.row
//        
//        selectedIndex = IndexPath(item: index, section: 0)
        
        let selectedCell = collectionView.cellForItem(at: indexPath) as? PaymentMethodCollectionViewCell
        
        selectedCell?.payModeImage.image = UIImage(named: payMethodImage[indexPath.row])
    }
    
    
}

extension PaymentMethodViewController {
    
    func loadDropDownMenuView() {
        
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
       
            //self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.accountCurrencyView.frame.width, height: self.accountCurrencyView.frame.height)
            if tappedCurrency == true {
                self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.accountCurrencyView.frame.width, height: self.accountCurrencyView.frame.height)
                self.accountCurrencyView.addSubview(self.dropDownMenuView!)
                
            } else {
                
                self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.payCurrencyView.frame.width, height: self.payCurrencyView.frame.height)
                self.payCurrencyView.addSubview(self.dropDownMenuView!)
            }
            
            self.dropDownMenuView?.selectedValue = { name, status in
                
                if self.tappedCurrency == true {
                    self.accountCurrencyButton.setTitle(name, for: .normal)
                } else {
                    self.payCurrencyButton.setTitle(name, for: .normal)
                }
                
                
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            
            //self.accountCurrencyView.addSubview(self.dropDownMenuView!)
        }
    }
    
    func dismissDropDownMenu() {
        if tappedCurrency == true {
            UIView.animate(withDuration: 0.2) {
                self.accountCurrencyHeightAnchor?.isActive = false
                self.accountCurrencyHeightAnchor = self.accountCurrencyView.heightAnchor.constraint(equalToConstant: 0)
                self.accountCurrencyHeightAnchor?.isActive = true
                
                self.view.layoutIfNeeded()
                self.dropDownMenuView?.titleView.isHidden = true
                
            } completion: { _ in
                self.removeDropDownMenuView()
                self.dropDownMenuView?.titleView.isHidden = false
            }
            
        } else {
            UIView.animate(withDuration: 0.2) {
                self.payCurrencyHeightAnchor?.isActive = false
                self.payCurrencyHeightAnchor = self.payCurrencyView.heightAnchor.constraint(equalToConstant: 0)
                self.payCurrencyHeightAnchor?.isActive = true
                
                self.view.layoutIfNeeded()
                self.dropDownMenuView?.titleView.isHidden = true
                
            } completion: { _ in
                self.removeDropDownMenuView()
                self.dropDownMenuView?.titleView.isHidden = false
            }
            
        }
        
        
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
}
