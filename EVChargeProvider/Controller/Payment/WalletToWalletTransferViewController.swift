//
//  WalletToWalletTransferViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import UIKit

class WalletToWalletTransferViewController: UIViewController {

    // Navigation
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navigationTitle: UILabel!
    
    @IBOutlet weak var receiverLabel: UILabel!
    @IBOutlet weak var receiverName: UITextField!
    @IBOutlet weak var receiverUserId: UITextField!
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var senderRemarks: UITextField!
    @IBOutlet weak var amountView: UIView!
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var currencyButton: UIButton!
    
    //XIB
    var chargingReceiptView: ChargingReceiptView?
    var dropDownMenuView: DropDownMenuView?
    
    let currencyView = UIView()
    var currencyHeightAnchor: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()
        onClickButtonAction()
        
        // For MenuButton First action
        if #available(iOS 14.0, *) {
            currencyButtonAction()
        } else {
            //
        }
    }
    
    func initialLoad() {
        // For Currency DropDown
        currencyView.translatesAutoresizingMaskIntoConstraints = false
        currencyView.backgroundColor = .gray
        view.addSubview(currencyView)
        
        NSLayoutConstraint.activate([
            currencyView.topAnchor.constraint(equalTo: amountView.bottomAnchor, constant: 0),
            currencyView.widthAnchor.constraint(equalToConstant: 200),
            currencyView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
            
        ])
    }
  
    func onClickButtonAction() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        currencyButton.addTarget(self, action: #selector(currencyButtonAction), for: .touchUpInside)
    }
    
    func setDesignStyle() {
        receiverName.setTextFieldCornorRadiusWithBorder(radius: 5, width: 2, color: .green)
        receiverUserId.setTextFieldCornorRadiusWithBorder(radius: 5, width: 2, color: .green)
        senderRemarks.setTextFieldCornorRadiusWithBorder(radius: 5, width: 2, color: .green)
        amountView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        submitButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        cancelButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        currencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
        currencyButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        
    }
    
    @objc func backButtonAction() {
        dismiss(animated: true)
    }
    
    @objc func currencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: currencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            UIView.animate(withDuration: 0.2) {
                self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.6)
                self.currencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()

            }
        }
    }
}

extension WalletToWalletTransferViewController {
    
    func loadDropDownMenuView() {
        
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
       
            self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.currencyView.frame.width, height: self.currencyView.frame.height)
            self.dropDownMenuView?.selectedValue = { name, status in
                self.currencyButton.setTitle(name, for: .normal)
                
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            
            self.currencyView.addSubview(self.dropDownMenuView!)
        }
    }
    
    func dismissDropDownMenu() {
        UIView.animate(withDuration: 0.2) {
            self.currencyHeightAnchor?.isActive = false
            self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalToConstant: 0)
            self.currencyHeightAnchor?.isActive = true
            
            self.view.layoutIfNeeded()
            self.dropDownMenuView?.titleView.isHidden = true
            
        } completion: { _ in
            self.removeDropDownMenuView()
            self.dropDownMenuView?.titleView.isHidden = false
        }
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
    
}
