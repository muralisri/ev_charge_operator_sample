//
//  MyEarningsViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import UIKit
import EventKit

class MyEarningsViewController: UIViewController {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var chargeSession: UILabel!
    @IBOutlet weak var sessionCount: UILabel!
    
    //Earning
    @IBOutlet weak var totalEarningView: UIView!
    @IBOutlet weak var totalEarningLabel: UILabel!
    @IBOutlet weak var totalEarningAmount: UILabel!
    
    //Revenue
    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var revenueLabel: UILabel!
    @IBOutlet weak var revenueAmount: UILabel!
    
    // Cancel Orders
    @IBOutlet weak var cancelOrdersView: UIView!
    @IBOutlet weak var cancelOrdersLabel: UILabel!
    @IBOutlet weak var cancelOrdersAmount: UILabel!

    @IBOutlet weak var sessionButton: UIButton!
    @IBOutlet weak var currencyButton: UIButton!
    
    @IBOutlet weak var calenderView: UIView!
    @IBOutlet weak var calenderHeightConstrain: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    //XIB
    var dropDownMenuView: DropDownMenuView?
    
    let currencyView = UIView()
    var currencyHeightAnchor: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDesignStyle()
        initialLoad()
        onClickButtonAction()
        

        
        // For MenuButton First action
        if #available(iOS 14.0, *) {
            currencyButtonAction()
        } else {
            //
        }
    }
    
    func initialLoad() {
        // For Currency DropDown
        currencyView.backgroundColor = .clear
        currencyView.translatesAutoresizingMaskIntoConstraints = false
        currencyView.backgroundColor = .gray
        view.addSubview(currencyView)
        
        NSLayoutConstraint.activate([
            currencyView.topAnchor.constraint(equalTo: currencyButton.bottomAnchor, constant: 0),
            currencyView.widthAnchor.constraint(equalToConstant: 200),
            currencyView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
            
        ])
        
       // calenderView.backgroundColor = .lightGray
        calenderView.setViewCornorRadiusWithShadow(radius: 15)
        calenderHeightConstrain.constant = 0
        datePicker.isHidden = true
        datePicker.datePickerMode = .date
        
    }
    
    func setDesignStyle() {
        totalEarningView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        revenueView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        cancelOrdersView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        currencyButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        currencyView.setViewCornerWithRadiusAndNoBorder(radius: 10)
    }
    
    func onClickButtonAction() {
        sessionButton.addTarget(self, action: #selector(sessionButtonAction), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        currencyButton.addTarget(self, action: #selector(currencyButtonAction), for: .touchUpInside)
        datePicker.addTarget(self, action: #selector(datePickerAction(_:)), for: .valueChanged)
    }
    
    @objc func backButtonAction() {
        dismiss(animated: true)
    }
    
    @objc func currencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: currencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            UIView.animate(withDuration: 0.2) {
                self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
                self.currencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()

            }
        }
        
    }
    
    @objc func sessionButtonAction() {
        UIView.animate(withDuration: 0.3) {
            self.datePicker.isHidden = false
            self.calenderHeightConstrain.constant = 250
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func datePickerAction(_ sender: UIDatePicker) {
        UIView.animate(withDuration: 0.3) {
            self.calenderHeightConstrain.constant = 0
            self.view.layoutIfNeeded()
            self.datePicker.isHidden = true
        } completion: { _ in
            
        }
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "dd-MM-yyyy"
        let selectedDate = dateFormate.string(from: sender.date)
        
        print("Selecte Date Wheel \(selectedDate)")
        sessionButton.setTitle(selectedDate, for: .normal)
    }
   
}

//extension MyEarningsViewController:

// MARK: Add UIView
extension MyEarningsViewController {
    
    func loadDropDownMenuView() {
        
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
       
            self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.currencyView.frame.width, height: self.currencyView.frame.height)
            self.dropDownMenuView?.selectedValue = { name, status in
                self.currencyButton.setTitle(name, for: .normal)
                
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            
            self.currencyView.addSubview(self.dropDownMenuView!)
        }
    }
    
    func dismissDropDownMenu() {
        UIView.animate(withDuration: 0.2) {
            self.currencyHeightAnchor?.isActive = false
            self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalToConstant: 0)
            self.currencyHeightAnchor?.isActive = true
            
            self.view.layoutIfNeeded()
            self.dropDownMenuView?.titleView.isHidden = true
            
        } completion: { _ in
            self.removeDropDownMenuView()
            self.dropDownMenuView?.titleView.isHidden = false
        }
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
}
