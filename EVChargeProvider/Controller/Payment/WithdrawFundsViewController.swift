//
//  WithdrawFundsViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 10/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class WithdrawFundsViewController: UIViewController {
    
    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // Wallet Balance
    @IBOutlet weak var walletBalanceView: UIView!
    @IBOutlet weak var walletBalanceLabel: UILabel!
    @IBOutlet weak var walletBalanceAmount: UILabel!
    @IBOutlet weak var walletCurrencyButton: UIButton!
    
    // Available Balance
    @IBOutlet weak var availableBalanceView: UIView!
    @IBOutlet weak var availableBalanceLabel: UILabel!
    @IBOutlet weak var availableBalanceAmount: UILabel!
    @IBOutlet weak var availableCurrencyButton: UIButton!
    
    
    // Amount
    @IBOutlet weak var addAmountView: UIView!
   // @IBOutlet weak var enterAmountLabel: UILabel!
    @IBOutlet weak var enterAmountTextField: UITextField!
    @IBOutlet weak var addCurrencyButton: UIButton!
    @IBOutlet weak var twentyKButton: UIButton!
    @IBOutlet weak var fiftyKButton: UIButton!
    @IBOutlet weak var hundredKButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    // Colletion View
    @IBOutlet weak var payMethodCollectionView: UICollectionView!
    
    let payMethodImage = ["walletMoney", "flutterWay", "applePay", "gpay", "paypal",]
    let payMethodImageWhite = ["walletMoneyWhite", "flutterWay", "applePayWhite", "gpayWhite", "paypalWhite"]
    
    var selectedIndex: IndexPath?
    
    var tappedCurrency = 0
    
    // XIB
    var dropDownMenuView: DropDownMenuView?
    
    let walletCurrencyView = UIView()
    let availableCurrencyView = UIView()
    let addCurrencyView = UIView()
    var walletCurrencyHeightAnchor: NSLayoutConstraint?
    var availableCurrencyHeightAnchor: NSLayoutConstraint?
    var addCurrencyHeightAnchor: NSLayoutConstraint?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()
        onClickButtonAction()
        
        // ColletionView Delegate
        payMethodCollectionView.dataSource = self
        payMethodCollectionView.delegate = self
        
        // For MenuButton First action
        if #available(iOS 14.0, *) {
            walletCurrencyButtonAction()
            availableCurrencyButtonAction()
            addCurrencyButtonAction()
        } else {
            //
        }
        
        
    }
    
    func initialLoad() {
        
        // Payment Method Cell
        let nib1 = UINib(nibName: "PaymentMethodCollectionViewCell", bundle: nil)
        payMethodCollectionView.register(nib1, forCellWithReuseIdentifier: "PayModeCell")
               
        walletCurrencyView.translatesAutoresizingMaskIntoConstraints = false
        walletCurrencyView.backgroundColor = .clear
        view.addSubview(walletCurrencyView)
        
        NSLayoutConstraint.activate([
            walletCurrencyView.topAnchor.constraint(equalTo: walletBalanceView.bottomAnchor, constant: 0),
            walletCurrencyView.widthAnchor.constraint(equalToConstant: 200),
            walletCurrencyView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
        ])
        
    
        availableCurrencyView.translatesAutoresizingMaskIntoConstraints = false
        availableCurrencyView.backgroundColor = .clear
        view.addSubview(availableCurrencyView)
        
        NSLayoutConstraint.activate([
            availableCurrencyView.topAnchor.constraint(equalTo: availableBalanceView.bottomAnchor, constant: 0),
            availableCurrencyView.widthAnchor.constraint(equalToConstant: 200),
            availableCurrencyView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 20)
        ])
        
        addCurrencyView.translatesAutoresizingMaskIntoConstraints = false
        addCurrencyView.backgroundColor = .clear
        view.addSubview(addCurrencyView)
        
        NSLayoutConstraint.activate([
            addCurrencyView.topAnchor.constraint(equalTo: addAmountView.bottomAnchor, constant: 0),
            addCurrencyView.widthAnchor.constraint(equalToConstant: 200),
            addCurrencyView.centerXAnchor.constraint(equalTo: addAmountView.centerXAnchor, constant: 0)
        ])

        
    }
    
     func onClickButtonAction() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        walletCurrencyButton.addTarget(self, action: #selector(walletCurrencyButtonAction), for: .touchUpInside)
        availableCurrencyButton.addTarget(self, action: #selector(availableCurrencyButtonAction), for: .touchUpInside)
         addCurrencyButton.addTarget(self, action: #selector(addCurrencyButtonAction), for: .touchUpInside)
        
    }
    
    @objc func backButtonAction() {
        
        dismiss(animated: true)
    }
    
    func setDesignStyle() {
        
        walletBalanceView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        walletCurrencyButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        availableBalanceView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        availableCurrencyButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        addAmountView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        addCurrencyButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        twentyKButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        fiftyKButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        hundredKButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        submitButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        walletCurrencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
        availableCurrencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
        
    }
    
    @objc func myEarningButtonAction() {
        push(controller: "MyEarningsViewController", animated: true)
    }
    
    @objc func walletToWalletButtonAction() {
        push(controller: "WalletToWalletTransferViewController", animated: true)
    }
    
    @objc func depositFundsButtonAction() {
        push(controller: "PaymentMethodViewController", animated: true)
    }
    
    @objc func withdrawFundsButtonAction() {
        
    }
    
    @objc func walletCurrencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: walletCurrencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            tappedCurrency = 0
            dismissDropDownMenu()
            UIView.animate(withDuration: 0.2) {
                self.walletCurrencyHeightAnchor = self.walletCurrencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
                self.walletCurrencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()
            }
        }
        
  
        
    }
    
    @objc func availableCurrencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: availableCurrencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            tappedCurrency = 1
            dismissDropDownMenu()
            UIView.animate(withDuration: 0.2) {
                self.availableCurrencyHeightAnchor = self.availableCurrencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
                self.availableCurrencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()
            }
        }
        
        
    }
    
    @objc func addCurrencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: addCurrencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            tappedCurrency = 2
            dismissDropDownMenu()
            UIView.animate(withDuration: 0.2) {
                self.addCurrencyHeightAnchor = self.addCurrencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
                self.addCurrencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()
            }
        }
        
    }
    
}

extension WithdrawFundsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return payMethodImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PayModeCell", for: indexPath) as? PaymentMethodCollectionViewCell
        
        //
        cell?.payModeImage.image = UIImage(named: payMethodImage[indexPath.row])
        
        let orangeColor = UIColor(red: 255/255, green: 105/255, blue: 36/255, alpha: 1)
        let backgroundView = UIView()
        backgroundView.backgroundColor = orangeColor
        cell?.selectedBackgroundView = backgroundView
        
        cell?.selectedBackgroundView?.layer.cornerRadius = 20
        cell?.selectedBackgroundView?.layer.masksToBounds = true
        
        return cell!
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        let index = indexPath.row
        //        selectedIndex = IndexPath(item: index, section: 0)
        
        let selectedCell = collectionView.cellForItem(at: indexPath) as? PaymentMethodCollectionViewCell
        
        selectedCell?.payModeImage.image = UIImage(named: payMethodImageWhite[indexPath.row])
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        //        let index = indexPath.row
        //
        //        selectedIndex = IndexPath(item: index, section: 0)
        
        let selectedCell = collectionView.cellForItem(at: indexPath) as? PaymentMethodCollectionViewCell
        
        selectedCell?.payModeImage.image = UIImage(named: payMethodImage[indexPath.row])
    }
    
    
    
    
}


extension WithdrawFundsViewController {
    func loadDropDownMenuView() {
        
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
            if tappedCurrency == 0 {
                self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.walletCurrencyView.frame.width, height: self.walletCurrencyView.frame.height)
                self.walletCurrencyView.addSubview(self.dropDownMenuView!)
                
            } else if tappedCurrency == 1{
                self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.availableCurrencyView.frame.width, height: self.availableCurrencyView.frame.height)
                self.availableCurrencyView.addSubview(self.dropDownMenuView!)
            } else {
                self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.addCurrencyView.frame.width, height: self.addCurrencyView.frame.height)
                self.addCurrencyView.addSubview(self.dropDownMenuView!)
            }
            
            self.dropDownMenuView?.selectedValue = { name, status in
                
                if self.tappedCurrency == 0{
                    self.walletCurrencyButton.setTitle(name, for: .normal)
                } else if self.tappedCurrency == 1{
                    self.availableCurrencyButton.setTitle(name, for: .normal)
                } else {
                    self.addCurrencyButton.setTitle(name, for: .normal)
                }
                
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            
            
        }
    }
    
    func dismissDropDownMenu() {
        if tappedCurrency == 0{
            UIView.animate(withDuration: 0.2) {
                self.walletCurrencyHeightAnchor?.isActive = false
                self.walletCurrencyHeightAnchor = self.walletCurrencyView.heightAnchor.constraint(equalToConstant: 0)
                self.walletCurrencyHeightAnchor?.isActive = true

                self.view.layoutIfNeeded()
                self.dropDownMenuView?.titleView.isHidden = true

            } completion: { _ in
                self.removeDropDownMenuView()
                self.dropDownMenuView?.titleView.isHidden = false
            }

        } else if tappedCurrency == 1 {
            UIView.animate(withDuration: 0.2) {
                self.availableCurrencyHeightAnchor?.isActive = false
                self.availableCurrencyHeightAnchor = self.walletCurrencyView.heightAnchor.constraint(equalToConstant: 0)
                self.availableCurrencyHeightAnchor?.isActive = true

                self.view.layoutIfNeeded()
                self.dropDownMenuView?.titleView.isHidden = true

            } completion: { _ in
                self.removeDropDownMenuView()
                self.dropDownMenuView?.titleView.isHidden = false
            }

        } else {
            UIView.animate(withDuration: 0.2) {
                self.addCurrencyHeightAnchor?.isActive = false
                self.addCurrencyHeightAnchor = self.addCurrencyView.heightAnchor.constraint(equalToConstant: 0)
                self.addCurrencyHeightAnchor?.isActive = true

                self.view.layoutIfNeeded()
                self.dropDownMenuView?.titleView.isHidden = true

            } completion: { _ in
                self.removeDropDownMenuView()
                self.dropDownMenuView?.titleView.isHidden = false
            }
        }
        
        
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
}
