//
//  AddCardViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 23/01/22.
//

import UIKit

class AddCardViewController: UIViewController {

    
    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var cardHolderNameLabel: UILabel!
    @IBOutlet weak var cardHolderName: UITextField!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardNumber: UITextField!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var expiryDate: UITextField!
    @IBOutlet weak var cvvLabel: UILabel!
    @IBOutlet weak var cvvInfoButton: UIButton!
    @IBOutlet weak var cvv: UITextField!
    @IBOutlet weak var addCardButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
        setDesignStyle()
    }
    
    func initialLoad() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        addCardButton.addTarget(self, action: #selector(addCardButtonAction), for: .touchUpInside)
    }
     
    func setDesignStyle() {
        
        cardHolderName.setTextFieldCornorRadiusWithBorder(radius: 7, width: 2, color: .green)
        cardNumber.setTextFieldCornorRadiusWithBorder(radius: 7, width: 2, color: .green)
        expiryDate.setTextFieldCornorRadiusWithBorder(radius: 7, width: 2, color: .green)
        cvv.setTextFieldCornorRadiusWithBorder(radius: 7, width: 2, color: .green)
        
        addCardButton.setButtonRoundCornerWithShadow(style: .normal, radius: 5)
        
    }

    @objc func backButtonAction() {
        dismiss(animated: true)
    }
    
    @objc func addCardButtonAction() {
        
    }
}
