//
//  WalletViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import UIKit

class WalletViewController: UIViewController {
    
    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // Wallet Balance
    @IBOutlet weak var walletBalanceView: UIView!
    @IBOutlet weak var walletBalanceLabel: UILabel!
    @IBOutlet weak var walletBalanceAmount: UILabel!
    @IBOutlet weak var walletCurrencyButton: UIButton!
    
    // Available Balance
    @IBOutlet weak var availableBalanceView: UIView!
    @IBOutlet weak var availableBalanceLabel: UILabel!
    @IBOutlet weak var availableBalanceAmount: UILabel!
    @IBOutlet weak var availableCurrencyButton: UIButton!
    
    // Buttons
    @IBOutlet weak var myEarningButton: UIButton!
    @IBOutlet weak var walletToWalletButton: UIButton!
    @IBOutlet weak var depositFundsButton: UIButton!
    @IBOutlet weak var withdrawFundsButton: UIButton!
    
    @IBOutlet weak var myEarningView: UIView!
    @IBOutlet weak var walletToWalletView: UIView!
    @IBOutlet weak var depositFundsView: UIView!
    @IBOutlet weak var withdrawFundsView: UIView!
    
    var tappedCurrency = true
    
    // XIB
    var dropDownMenuView: DropDownMenuView?
    
    let walletCurrencyView = UIView()
    let availableCurrencyView = UIView()
    var walletCurrencyHeightAnchor: NSLayoutConstraint?
    var availableCurrencyHeightAnchor: NSLayoutConstraint?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
        setDesignStyle()
        onClickButtonaAction()
        
        // For MenuButton First action
        if #available(iOS 14.0, *) {
            walletCurrencyButtonAction()
            availableCurrencyButtonAction()
        } else {
            //
        }
    }
    
    func initialLoad() {
        walletCurrencyView.translatesAutoresizingMaskIntoConstraints = false
        walletCurrencyView.backgroundColor = .clear
        view.addSubview(walletCurrencyView)
        
        NSLayoutConstraint.activate([
            walletCurrencyView.topAnchor.constraint(equalTo: walletBalanceView.bottomAnchor, constant: 0),
            walletCurrencyView.widthAnchor.constraint(equalToConstant: 200),
            walletCurrencyView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
        ])
        
    
        availableCurrencyView.translatesAutoresizingMaskIntoConstraints = false
        availableCurrencyView.backgroundColor = .clear
        view.addSubview(availableCurrencyView)
        
        NSLayoutConstraint.activate([
            availableCurrencyView.topAnchor.constraint(equalTo: availableBalanceView.bottomAnchor, constant: 0),
            availableCurrencyView.widthAnchor.constraint(equalToConstant: 200),
            availableCurrencyView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 20)
        ])
    }
    
    func onClickButtonaAction() {
        // Button Action
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        myEarningButton.addTarget(self, action: #selector(myEarningButtonAction), for: .touchUpInside)
        walletToWalletButton.addTarget(self, action: #selector(walletToWalletButtonAction), for: .touchUpInside)
        depositFundsButton.addTarget(self, action: #selector(depositFundsButtonAction), for: .touchUpInside)
        withdrawFundsButton.addTarget(self, action: #selector(withdrawFundsButtonAction), for: .touchUpInside)
        walletCurrencyButton.addTarget(self, action: #selector(walletCurrencyButtonAction), for: .touchUpInside)
        availableCurrencyButton.addTarget(self, action: #selector(availableCurrencyButtonAction), for: .touchUpInside)
    
    }
    
    func setDesignStyle() {
        walletBalanceView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        walletCurrencyButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        availableBalanceView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        availableCurrencyButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        myEarningView.setViewCornorRadiusWithShadow(radius: 10)
        walletToWalletView.setViewCornorRadiusWithShadow(radius: 10)
        depositFundsView.setViewCornorRadiusWithShadow(radius: 10)
        withdrawFundsView.setViewCornorRadiusWithShadow(radius: 10)
        walletCurrencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
        availableCurrencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
        
    }
    
    @objc func backButtonAction() {
        dismissDropDownMenu()
        dismiss(animated: true)
    }
    
    @objc func walletCurrencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: walletCurrencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            tappedCurrency = true
                    UIView.animate(withDuration: 0.2) {
                        self.walletCurrencyHeightAnchor = self.walletCurrencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
                        self.walletCurrencyHeightAnchor?.isActive = true
                        self.view.layoutIfNeeded()
                        self.loadDropDownMenuView()
                    }
        }

        
    }
    
    @objc func availableCurrencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: availableCurrencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            tappedCurrency = false
            UIView.animate(withDuration: 0.2) {
                self.availableCurrencyHeightAnchor = self.availableCurrencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
                self.availableCurrencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()
            }
        }
  
    }
    
    @objc func myEarningButtonAction() {
        dismissDropDownMenu()
        push(controller: "MyEarningsViewController", animated: true)
    }
    
    @objc func walletToWalletButtonAction() {
        dismissDropDownMenu()
        push(controller: "WalletToWalletTransferViewController", animated: true)
    }
    
    @objc func depositFundsButtonAction() {
        dismissDropDownMenu()
        push(controller: "DepositeFundsViewController", animated: true)
    }
    
    @objc func withdrawFundsButtonAction() {
        dismissDropDownMenu()
        push(controller: "WithdrawFundsViewController", animated: true)
    }

}

// MARK: ADD UIView
extension WalletViewController {
    
    func loadDropDownMenuView() {
        
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
            if tappedCurrency == true {
                self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.walletCurrencyView.frame.width, height: self.walletCurrencyView.frame.height)
                self.walletCurrencyView.addSubview(self.dropDownMenuView!)
                
            } else {
                self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.availableCurrencyView.frame.width, height: self.availableCurrencyView.frame.height)
                self.availableCurrencyView.addSubview(self.dropDownMenuView!)
            }
            
            self.dropDownMenuView?.selectedValue = { name, status in
                
                if self.tappedCurrency == true {
                    self.walletCurrencyButton.setTitle(name, for: .normal)
                } else {
                    self.availableCurrencyButton.setTitle(name, for: .normal)
                }
                
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            
            
        }
    }
    
    func dismissDropDownMenu() {
        
        if tappedCurrency == true {
            UIView.animate(withDuration: 0.2) {
                self.walletCurrencyHeightAnchor?.isActive = false
                self.walletCurrencyHeightAnchor = self.walletCurrencyView.heightAnchor.constraint(equalToConstant: 0)
                self.walletCurrencyHeightAnchor?.isActive = true
                
                self.view.layoutIfNeeded()
                self.dropDownMenuView?.titleView.isHidden = true
                
            } completion: { _ in
                self.removeDropDownMenuView()
                self.dropDownMenuView?.titleView.isHidden = false
            }
            
        } else {
            UIView.animate(withDuration: 0.2) {
                self.availableCurrencyHeightAnchor?.isActive = false
                self.availableCurrencyHeightAnchor = self.walletCurrencyView.heightAnchor.constraint(equalToConstant: 0)
                self.availableCurrencyHeightAnchor?.isActive = true
                
                self.view.layoutIfNeeded()
                self.dropDownMenuView?.titleView.isHidden = true
                
            } completion: { _ in
                self.removeDropDownMenuView()
                self.dropDownMenuView?.titleView.isHidden = false
            }
            
        }
        
        
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
}
