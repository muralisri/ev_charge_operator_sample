//
//  AttachDocumentsViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 18/01/22.
//

import UIKit

class AttachDocumentsViewController: UIViewController {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var documentsHoldingView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var documentTable: UITableView!
    
    // Variables
    
    let identityName = ["Proof of Identity", "Academic Documents", "Driver's License"]
    let identityDescription = ["National ID, Passport, Driver's License", "Highest Academic Certificate" , ""]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setDesignStyle()
        initialLoad()
    }
    
    func initialLoad() {
        
        documentTable.delegate = self
        documentTable.dataSource = self
        documentTable.separatorStyle = .none
        
        let nib = UINib(nibName: "AttachDocumentsTableViewCell", bundle: nil)
        documentTable.register(nib, forCellReuseIdentifier: "DocumentCell")
        
        signupButton.addTarget(self, action: #selector(signupButtonAction), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
              
    }
    
    func setDesignStyle() {
        
        documentsHoldingView.setViewCornerRadiusWithBorder(radius: 7, width: 3, color: .green)
        cancelButton.setButtonRoundCornerWithShadow(style: .normal, radius: 5)
        saveButton.setButtonRoundCornerWithShadow(style: .normal, radius: 5)
        
        signupButton.setButtonRoundCornerWithShadow(style: .normal, radius: 5)
        
    }
    
    @objc func signupButtonAction() {
    
        push(controller: "SignInViewController", animated: true)
        
    }
    
    @objc func backButtonAction() {
        dismiss(animated: true)
    }
    

}

extension AttachDocumentsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return identityName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentCell", for: indexPath) as? AttachDocumentsTableViewCell
        
        cell?.imageUploaderView.setViewRoundCornor()
        cell?.identityName.text = identityName[indexPath.row]
        cell?.identityDescription.text = identityDescription[indexPath.row]
        
        return cell!
    }
}
