//
//  SignUpViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import UIKit

class SignUpViewController: UIViewController {

   // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // Profile
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var cameraOuterView: UIView!
    @IBOutlet weak var cameraInnerView: UIView!
 
    // Details
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var chargePoint: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmationPassword: UITextField!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var eyeButton: UIButton!
    
    @IBOutlet weak var selectTypeButton: UIButton!
    @IBOutlet weak var selectTypeView: UIView!
    @IBOutlet weak var dialCodeLable: UILabel!
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var countryView: UIView!
    
    // Variable
    var operatorType: Int?
    
    let currencyView = UIView()
    var currencyHeightAnchor: NSLayoutConstraint?
    
    var dropDownMenuView: DropDownMenuView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
        setDesignStyle()
        onClickButtonAction()
        
        // First Action - For Select type menu button
        if #available(iOS 14.0, *) {
        selectTypeButtonAction()
        } else {
            //
        }
        
        
    }
 
    func initialLoad() {
        // For Currency DropDown
        currencyView.translatesAutoresizingMaskIntoConstraints = false
        currencyView.backgroundColor = .gray
        view.addSubview(currencyView)
       
        
        
        NSLayoutConstraint.activate([
            currencyView.topAnchor.constraint(equalTo: selectTypeView.bottomAnchor, constant: 0),
            currencyView.widthAnchor.constraint(equalToConstant: 200),
            currencyView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10)
            
            
        ])
    }
    
    func onClickButtonAction() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        selectTypeButton.addTarget(self, action: #selector(selectTypeButtonAction), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(nextButtonAction), for: .touchUpInside)
    }
    
    
    func setDesignStyle() {
        profileImage.setImageCornorRoundWithGreenBorder(width: 2)
        cameraOuterView.setViewRoundCornor()
        cameraInnerView.setViewRoundCornor()
        
        nextButton.setButtonCornorRadiusWithGreenBorder(radius: 5, width: 0)
        
        // TextField
        firstName.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
        lastName.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
        address.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
        chargePoint.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
        emailAddress.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
        countryView.setViewCornerRadiusWithBorder(radius: 7, width: 1, color: .green)
        phoneNumberView.setViewCornerRadiusWithBorder(radius: 7, width: 1, color: .green)
        passwordView.setViewCornerRadiusWithBorder(radius: 7, width: 1, color: .green)
        confirmationPassword.setTextFieldCornorRadiusWithBorder(radius: 5, width: 1, color: .green)
        selectTypeView.setViewCornerRadiusWithBorder(radius: 7, width: 1, color: .green)
        currencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
        
    }
    
    @objc func backButtonAction() {
        dismiss(animated: true)
        
        
    }
    
    @objc func selectTypeButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.operatorTyps, menuTitle: "Operator type", field: selectTypeButton, completion: { title in

                print("Selected value ... \(title)")

                if title == "Mobile" {
                    self.operatorType = 0
                    UserDefaults.standard.set(0, forKey: "operator_type")
                } else {
                    self.operatorType = 1
                    UserDefaults.standard.set(1, forKey: "operator_type")
                }
            })
        } else {
            UIView.animate(withDuration: 0.2) {
                self.currencyHeightAnchor?.isActive = false
                self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalToConstant: 150)
                self.currencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()
            }
        }
        
        
        
    }
    
    @objc func nextButtonAction() {
        
        if operatorType == nil {
            
            showAlert(message: "Please Select One Operator")
            
        } else {
            
            push(controller: "AttachDocumentsViewController", animated: true)
        }
        
        
    }
    

}

extension SignUpViewController {
    func loadDropDownMenuView() {
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
       
            self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.currencyView.frame.width, height: self.currencyView.frame.height)
            self.dropDownMenuView?.currencyList = ["Mobile", "Fixed"]
            self.dropDownMenuView?.selectedValue = { name, status in
                self.selectTypeButton.setTitle(name, for: .normal)
                
                if name == "Mobile" {
                    self.operatorType = 0
                    UserDefaults.standard.set(0, forKey: "operator_type")
                } else {
                    self.operatorType = 1
                    UserDefaults.standard.set(1, forKey: "operator_type")
                }
                
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            self.currencyView.addSubview(self.dropDownMenuView!)
        }
    }
    
    func dismissDropDownMenu() {
        UIView.animate(withDuration: 0.2) {
            self.currencyHeightAnchor?.isActive = false
            self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalToConstant: 0)
            self.currencyHeightAnchor?.isActive = true
            
            self.view.layoutIfNeeded()
            self.dropDownMenuView?.titleView.isHidden = true
            
        } completion: { _ in
            self.removeDropDownMenuView()
            self.dropDownMenuView?.titleView.isHidden = false
        }
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
}
