//
//  FilterSettingsViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 11/01/22.
//

import UIKit

class FilterSettingsViewController: UIViewController {

    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    var filterArray = ["Standard", "Charge Parameters", "Post Payment Method", "Distance", "Map"]

    var filterStandardView: FilterStandardView?
    var filterChargeParametersView: FilterChargeParametersView?
    var filterPostPaymentMethodView: FilterPostPaymentMethodView?
    var filterDistanceView: FilterDistanceView?
    var filterMapView: FilterMapView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        loadFilterCellNib()
        
        loadFilterStandardView()
        
        
        
    }
    
    func initialLoad() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
    
    @objc func backButtonAction() {
        
        dismiss(animated: true)
    }
    
    func loadFilterCellNib() {
        filterTableView.delegate = self
        filterTableView.dataSource = self
        filterTableView.separatorStyle = .none
       
        
        let nib = UINib(nibName: "FilterSettingTableViewCell", bundle: nil)
        filterTableView.register(nib, forCellReuseIdentifier: "FilterCell")
        
        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
        self.filterTableView.selectRow(at: indexPathForFirstRow, animated: true, scrollPosition: .none)
    }
    

}

extension FilterSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as? FilterSettingTableViewCell

         cell?.FilterNameList.text = filterArray[indexPath.row]
        
        
        // Set Selected cell background color
        let greenColor = UIColor(red: 164/255, green: 252/255, blue: 193/255, alpha: 1)
        let backgroundView = UIView()
                backgroundView.backgroundColor = greenColor
        cell?.selectedBackgroundView = backgroundView
        
  
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        switch index {
        case 0:
            
            loadFilterStandardView()
            removeFilterChargeParametersView()
            removeFilterPostPaymentMethodView()
            removeFilterDistanceView()
            removeFilterMapView()
            
        case 1:
            
            loadFilterChargeParametersView()
            
            removeFilterStandardView()
            removeFilterPostPaymentMethodView()
            removeFilterDistanceView()
            removeFilterMapView()
            
            
        case 2:
            
            loadFilterPostPaymentMethodView()
            
            removeFilterStandardView()
            removeFilterChargeParametersView()
            removeFilterDistanceView()
            removeFilterMapView()
            
        case 3:
            
            loadFilterDistanceView()
            
            removeFilterStandardView()
            removeFilterChargeParametersView()
            removeFilterPostPaymentMethodView()
            removeFilterMapView()
            
        case 4:
            
            loadFilterMapView()
            
            removeFilterStandardView()
            removeFilterChargeParametersView()
            removeFilterPostPaymentMethodView()
            removeFilterDistanceView()
            
        default:
            return
        }
    }
    
    
}


extension FilterSettingsViewController {
    
    
    func loadFilterStandardView() {
        if filterStandardView == nil, let bundle = Bundle.main.loadNibNamed("FilterStandardView", owner: self, options: nil)?.first as? FilterStandardView {
            
            self.filterStandardView = bundle
            self.filterStandardView?.frame = CGRect(x: 0, y: 0, width: self.rightView.frame.width, height: self.rightView.frame.height)
            
            self.rightView.addSubview(self.filterStandardView!)
    }

    }
    
    func removeFilterStandardView() {
        filterStandardView?.removeFromSuperview()
        filterStandardView = nil
    }
    
    
    func loadFilterChargeParametersView() {
        if filterChargeParametersView == nil, let bundle = Bundle.main.loadNibNamed("FilterChargeParametersView", owner: self, options: nil)?.first as? FilterChargeParametersView {
            
            self.filterChargeParametersView = bundle
            self.filterChargeParametersView?.frame = CGRect(x: 0, y: 0, width: self.rightView.frame.width, height: self.rightView.frame.height)
            
            self.rightView.addSubview(self.filterChargeParametersView!)
    }

    }
    
    func removeFilterChargeParametersView() {
        filterChargeParametersView?.removeFromSuperview()
        filterChargeParametersView = nil
    }

    func loadFilterPostPaymentMethodView() {
        if filterPostPaymentMethodView == nil, let bundle = Bundle.main.loadNibNamed("FilterPostPaymentMethodView", owner: self, options: nil)?.first as? FilterPostPaymentMethodView {
            
            self.filterPostPaymentMethodView = bundle
            self.filterPostPaymentMethodView?.frame = CGRect(x: 0, y: 0, width: self.rightView.frame.width, height: self.rightView.frame.height)
            
            self.rightView.addSubview(self.filterPostPaymentMethodView!)
    }

    }
    
    func removeFilterPostPaymentMethodView() {
        filterPostPaymentMethodView?.removeFromSuperview()
        filterPostPaymentMethodView = nil
        
    }
    
    func loadFilterDistanceView() {
        if filterDistanceView == nil, let bundle = Bundle.main.loadNibNamed("FilterDistanceView", owner: self, options: nil)?.first as? FilterDistanceView {
            
            self.filterDistanceView = bundle
            self.filterDistanceView?.frame = CGRect(x: 0, y: 0, width: self.rightView.frame.width, height: self.rightView.frame.height)
            
            self.rightView.addSubview(self.filterDistanceView!)
    }

    }
    
    func removeFilterDistanceView() {
        
        filterDistanceView?.removeFromSuperview()
        filterDistanceView = nil
        
    }
    
    func loadFilterMapView() {
        if filterMapView == nil, let bundle = Bundle.main.loadNibNamed("FilterMapView", owner: self, options: nil)?.first as? FilterMapView {
            
            self.filterMapView = bundle
            self.filterMapView?.frame = CGRect(x: 0, y: 0, width: self.rightView.frame.width, height: self.rightView.frame.height)
            
            self.rightView.addSubview(self.filterMapView!)
    }

    }
    
    func removeFilterMapView() {
        
        filterMapView?.removeFromSuperview()
        filterMapView = nil
    }
    
}
