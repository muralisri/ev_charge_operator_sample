//
//  RequiredChargeViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 23/01/22.
//

import UIKit

class RequiredChargeViewController: UIViewController {

    //Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    // Account Balance
    @IBOutlet weak var accountBalance: UILabel!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var balanceAmount: UILabel!
    @IBOutlet weak var currencyButton: UIButton!
    
    // Required Charge
    @IBOutlet weak var requiredChargeView: UIView!
    @IBOutlet weak var requiredChargeLabel: UILabel!
    
    // -kWh
    @IBOutlet weak var kWhLabel: UILabel!
    @IBOutlet weak var kWhSliderView: UIView!
    @IBOutlet weak var kWhSlider: UISlider!
    @IBOutlet weak var kWhSliderValue: UILabel!
    
    // -Cost
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var costSlider: UISlider!
    @IBOutlet weak var costSliderView: UIView!
    @IBOutlet weak var costSliderValue: UILabel!
    @IBOutlet weak var submitButton: UIButton!
        
    // ViewModel
    var requiredChargeViewModel = RequiredChargeViewModel()
    
    // XIB
    var dropDownMenuView: DropDownMenuView?
    
    var apiManager = APIManager()
    
    let currencyView = UIView()
    var currencyHeightAnchor: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialLoad()
        setDesignStyle()
        onClickButtonAction()
        
        // For MenuButton First action
        if #available(iOS 14.0, *) {
            currencyButtonAction()
        } else {
            //
        }
        
    }
    
    func initialLoad() {
        // For Currency DropDown
        currencyView.translatesAutoresizingMaskIntoConstraints = false
        currencyView.backgroundColor = .gray
        view.addSubview(currencyView)
        
        NSLayoutConstraint.activate([
            currencyView.topAnchor.constraint(equalTo: balanceView.bottomAnchor, constant: 0),
            currencyView.widthAnchor.constraint(equalToConstant: 200),
            currencyView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
            
        ])
    }
  
    func onClickButtonAction() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        
        // Slider
        kWhSlider.addTarget(self, action: #selector(kWhSliderAction(_:)), for: .valueChanged)
        costSlider.addTarget(self, action: #selector(costSliderAction(_:)), for: .valueChanged)
        submitButton.addTarget(self, action: #selector(submitButtonAction), for: .touchUpInside)
        
        currencyButton.addTarget(self, action: #selector(currencyButtonAction), for: .touchUpInside)
    }
    
    func setDesignStyle() {
        
        balanceView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        requiredChargeView.setViewCornorRadiusWithShadow(radius: 20)
        requiredChargeLabel.setLableSomeCornerRadius(radius: 20)
        kWhSliderView.setViewCornorRadiusWithShadow(radius: 10)
        costSliderView.setViewCornorRadiusWithShadow(radius: 10)
        submitButton.setButtonRoundCornerWithShadow(style: .normal, radius: 5)
        currencyView.setViewCornerWithRadiusAndNoBorder(radius: 15)
        
    }
    
    @objc func backButtonAction() {
        dismissDropDownMenu()
        dismiss(animated: true)
    }
    
    @objc func submitButtonAction() {
        dismissDropDownMenu()
        push(controller: "PaymentMethodViewController", animated: true)
    }
    
    @objc func currencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: currencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            UIView.animate(withDuration: 0.2) {
                self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.8)
                self.currencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()

            }
        }
        
    }
    
    @objc func kWhSliderAction(_ slider: UISlider) {
        
        let kWhValue = kWhSlider.value
        costSlider.setValue(kWhValue, animated: true)
        kWhSliderValue.text = "kWh \(Int(kWhValue))"
        costSliderValue.text = "UGX \(Int(kWhValue)+500)"
   
    }
    
    @objc func costSliderAction(_ slider: UISlider) {
        
        let costValue = costSlider.value
        kWhSlider.setValue(costValue, animated: true)
        costSliderValue.text = "UGX \(Int(costValue)+500)"
        kWhSliderValue.text = "kWh \(Int(costValue))"
   
    }

}

// MARK: ADD UIView
extension RequiredChargeViewController {
    
    func loadDropDownMenuView() {
        
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
       
            self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.currencyView.frame.width, height: self.currencyView.frame.height)
            self.dropDownMenuView?.selectedValue = { name, status in
                self.currencyButton.setTitle(name, for: .normal)
                
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            
            self.currencyView.addSubview(self.dropDownMenuView!)
        }
    }
    
    func dismissDropDownMenu() {
        UIView.animate(withDuration: 0.2) {
            self.currencyHeightAnchor?.isActive = false
            self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalToConstant: 0)
            self.currencyHeightAnchor?.isActive = true
            
            self.view.layoutIfNeeded()
            self.dropDownMenuView?.titleView.isHidden = true
            
        } completion: { _ in
            self.removeDropDownMenuView()
            self.dropDownMenuView?.titleView.isHidden = false
        }
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
    
}
