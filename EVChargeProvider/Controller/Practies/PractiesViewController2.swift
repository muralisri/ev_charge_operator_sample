//
//  PractiesViewController2.swift
//  EVChargeProvider
//
//  Created by MURALI on 31/01/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class PractiesViewController2: UIViewController {

    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var sliderBar: UISlider!
    
    @IBOutlet weak var sliderBar2: UISlider!
    
    @IBOutlet weak var lblSliderValue: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sliderBar.addTarget(self, action: #selector(sliderBarAction(_:)), for: .valueChanged)
        
        sliderBar2.addTarget(self, action: #selector(sliderBar2Action(_:)), for: .valueChanged)
        
        sliderBar.setValue(20, animated: true)
        sliderBar.isContinuous = true
        
        let int = Int(valueTextField.text!)
        
        sliderBar.setValue(Float(int ?? 0), animated: true)

    }
    
    @objc func sliderBarAction(_ sender: UISlider) {
        
        sliderBar.isContinuous = true
        valueLabel.text = String(sliderBar.value)
        
      let sli =   sliderBar.trackRect(forBounds: sliderBar.bounds)
      let sli1 = sliderBar.thumbRect(forBounds: sliderBar.bounds, trackRect: sli, value: sliderBar.value)
        
        sliderBar2.setValue(sliderBar.value - 10, animated: true)
        
        valueLabel.center = CGPoint(x: sli.origin.x + sender.frame.origin.x + 8, y: sender.frame.origin.y + 20)
    }
    
    @objc func sliderBar2Action(_ sender: UISlider) {
        let x = Int(round(sender.value))
        sliderBar2.isContinuous = true
                lblSliderValue.text = "\(x)"
                lblSliderValue.center = setUISliderThumbValueWithLabel(slider: sender)
        
    }
    
    func setUISliderThumbValueWithLabel(slider: UISlider) -> CGPoint {
            let slidertTrack : CGRect = slider.trackRect(forBounds: slider.bounds)
            let sliderFrm : CGRect = slider .thumbRect(forBounds: slider.bounds, trackRect: slidertTrack, value: slider.value)
            return CGPoint(x: sliderFrm.origin.x + slider.frame.origin.x + 34, y: slider.frame.origin.y + 50)
        }

}
