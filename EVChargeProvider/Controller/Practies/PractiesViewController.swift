//
//  PractiesViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 25/01/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class PractiesViewController: UIViewController {

    var mobileChargePointOrderView: MobileChargePointOrderView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        
       
        
//        let story = UIStoryboard(name: "Main", bundle: nil)
//        let vc = story.instantiateViewController(withIdentifier: "PractiesViewController2") as? PractiesViewController2
//
//
//        self.present(vc!, animated: true, completion: nil)
        self.loadMobileChargePointOrderView()
     
        
    }
    
    func loadMobileChargePointOrderView() {
        
        if mobileChargePointOrderView == nil, let bundle = Bundle.main.loadNibNamed("MobileChargePointOrderView", owner: self, options: nil)?.first as? MobileChargePointOrderView {
            
            self.mobileChargePointOrderView = bundle
            self.mobileChargePointOrderView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.height)
            
            //self.mobileChargePointOrderView?.cancelButton.addTarget(self, action: #selector(OrderCancelButtonAction), for: .touchUpInside)
           // self.mobileChargePointOrderView?.confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
            
            self.view.addSubview(self.mobileChargePointOrderView!)
            
           // self.subview.addSubview(self.mobileChargePointOrderView!)
            
            //subview.addSubview(self.mobileChargePointOrderView!)
            
        }
    }
    
    func removeMobileChargePointOrderView() {
        self.mobileChargePointOrderView?.removeFromSuperview()
        self.mobileChargePointOrderView = nil
    }
    

   

}
