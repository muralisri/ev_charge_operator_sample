//
//  ScheduledFixedViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 01/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class ScheduledFixedChargingViewController: UIViewController {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var noValue: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateValue: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeValue: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var estKwhRequiedLabel: UILabel!
    @IBOutlet weak var estkKwhRequired: UILabel!
    @IBOutlet weak var chargeingStandardLabel: UILabel!
    @IBOutlet weak var chargingStandard: UILabel!
    @IBOutlet weak var bookingDurationLabel: UILabel!
    @IBOutlet weak var bookingDuration: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var carModel: UILabel!
    @IBOutlet weak var licensePlateNoLabel: UILabel!
    @IBOutlet weak var licensePlateNo: UILabel!
    @IBOutlet weak var estimatedCostLabel: UILabel!
    @IBOutlet weak var estimatedCost: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var phoneCallButton: UIButton!
    @IBOutlet weak var inAppCallButton: UIButton!
    @IBOutlet weak var completedView: UIView!
    @IBOutlet weak var requestorNoView: UIView!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var timeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()
    }
    
    func initialLoad() {
        
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
    
    func setDesignStyle() {
        statusButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        actionButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        phoneCallButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        inAppCallButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        dateView.setViewCornerRadiusWithBorder(radius: 5, width: 2, color: .green)
        timeView.setViewCornerRadiusWithBorder(radius: 5, width: 2, color: .green)
        noValue.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        userName.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        estkKwhRequired.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        chargingStandard.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        bookingDuration.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        carModel.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        licensePlateNo.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        estimatedCost.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        completedView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        requestorNoView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
    }
    
    @objc func backButtonAction() {
        
        dismiss(animated: true)
    }


}
