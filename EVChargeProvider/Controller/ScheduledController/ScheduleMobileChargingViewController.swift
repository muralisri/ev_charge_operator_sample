//
//  ScheduleMobileChargingViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 07/01/22.
//

import UIKit
import Lottie

class ScheduleMobileChargingViewController: UIViewController {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var noValueLabel: UILabel!
    @IBOutlet weak var dateValueLabel: UILabel!
    @IBOutlet weak var timeValueLabel: UILabel!
    @IBOutlet weak var chargePointValueLabel: UILabel!
    @IBOutlet weak var locationValueLabel: UILabel!
    @IBOutlet weak var kwhValueLabel: UILabel!
    @IBOutlet weak var chargingStandardValueLabel: UILabel!
    @IBOutlet weak var estimatedValueLabel: UILabel!
    @IBOutlet weak var cancelledButton: UIButton!
    @IBOutlet weak var phoneCallButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var inAppCallButton: UIButton!
    @IBOutlet weak var requestorNoView: UIView!
    
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var timeView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()
    }
    
    func initialLoad() {
        
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
    }
    
    func setDesignStyle() {
        cancelledButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        cancelButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        phoneCallButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        inAppCallButton.setButtonCornorWithRadiusAndNoBorder(radius: 5)
        
        dateView.setViewCornerRadiusWithBorder(radius: 5, width: 2, color: .green)
        timeView.setViewCornerRadiusWithBorder(radius: 5, width: 2, color: .green)
        
        noValueLabel.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)

        chargePointValueLabel.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        locationValueLabel.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        kwhValueLabel.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        chargingStandardValueLabel.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        estimatedValueLabel.setLabelCornerRadiusWithGreenBorder(radius: 5, width: 2)
        requestorNoView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
    }
    
    @objc func backButtonAction() {
        
        dismiss(animated: true)
    }

}
