//
//  FixedChargeTranscationsViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 29/01/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class FixedChargeingTranscationsViewController: UIViewController {

    // Navigation
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var fromView: UIView!
    @IBOutlet weak var fromButton: UIButton!
    
    @IBOutlet weak var toView: UIView!
    @IBOutlet weak var toButton: UIButton!
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var noValue: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateValue: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeValue: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var chargingStandLabel: UILabel!
    @IBOutlet weak var chargingStand: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var carModel: UILabel!
    @IBOutlet weak var licensePlateNoLabel: UILabel!
    @IBOutlet weak var licensePlateNo: UILabel!
    @IBOutlet weak var kWhLabel: UILabel!
    @IBOutlet weak var kWhValue: UILabel!
    @IBOutlet weak var chargeDurationLabel: UILabel!
    @IBOutlet weak var chargeDuration: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var completedView: UIView!
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var timeView: UIView!
    
    
    var chargingReceiptView: ChargingReceiptView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()
    }
    
    func initialLoad() {
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        viewButton.addTarget(self, action: #selector(viewButtonAction), for: .touchUpInside)
        
    }
    
    func setDesignStyle() {
        
        fromButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        toButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        statusButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        viewButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        noValue.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        userName.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        chargingStand.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        carModel.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        licensePlateNo.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        kWhValue.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        chargeDuration.setLabelCornerRadiusWithGreenBorder(radius: 7, width: 2)
        
        dateView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        timeView.setViewCornerRadiusWithBorder(radius: 7, width: 2, color: .green)
        
        fromView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        toView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        completedView.setViewCornerWithRadiusAndNoBorder(radius: 7)

    }
    
    @objc func backButtonAction() {
        dismiss(animated: true)
    }
    @objc func viewButtonAction() {
        //loadChargingReceiptView() 
    }
    
    
    func loadChargingReceiptView() {
        
        if chargingReceiptView == nil, let bundle = Bundle.main.loadNibNamed("ChargingReceiptView", owner: self, options: nil)?.first as? ChargingReceiptView {
            
            self.chargingReceiptView = bundle
            
            self.chargingReceiptView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            
            self.chargingReceiptView?.backButton.addTarget(self, action: #selector(receiptBackButtonAction), for: .touchUpInside)
            
            self.view.addSubview(self.chargingReceiptView!)
        }
    }
    
    func removeChargingReceiptView() {
        self.chargingReceiptView?.removeFromSuperview()
        self.chargingReceiptView = nil
    }
    
    @objc func receiptBackButtonAction() {
        removeChargingReceiptView()
    }
    

}
