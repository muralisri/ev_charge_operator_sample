//
//  OtpVerifyViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 08/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class OtpVerifyViewController: UIViewController {
// Navigation
    
    @IBOutlet weak var navigationTitle: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBOutlet weak var otpVerification: UILabel!
    
    @IBOutlet weak var otpDescription: UILabel!
    
    @IBOutlet weak var entereingOtpOne: UITextField!
    
    @IBOutlet weak var enteringOtpTwo: UITextField!
    
    @IBOutlet weak var enteringOtpThree: UITextField!
    
    @IBOutlet weak var enteringOtpFour: UITextField!
    
    @IBOutlet weak var didnotReceiveButton: UIButton!
    
    @IBOutlet weak var resendButton: UIButton!
    
    @IBOutlet weak var starChargingButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setDesignStyle()
        initialLoad()
    }
    
    func initialLoad() {
        
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        starChargingButton.addTarget(self, action: #selector(starChargingButtonAction), for: .touchUpInside)
    }
    
    func setDesignStyle() {
        
        starChargingButton.setButtonCornorWithRadiusAndNoBorder(radius: 7)
        
        entereingOtpOne.setTextFieldCornorRadiusWithBorder(radius: 7, width: 2, color: .orange)
        enteringOtpTwo.setTextFieldCornorRadiusWithBorder(radius: 7, width: 2, color: .orange)
        enteringOtpThree.setTextFieldCornorRadiusWithBorder(radius: 7, width: 2, color: .orange)
        enteringOtpFour.setTextFieldCornorRadiusWithBorder(radius: 7, width: 2, color: .orange)
    }
    
    @objc func backButtonAction() {
        
        dismiss(animated: true)
    }

    @objc func starChargingButtonAction() {
       push(controller: "ChargingInfoViewController", animated: true)
    }

}
