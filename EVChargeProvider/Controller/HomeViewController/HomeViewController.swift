//
//  HomeViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 08/01/22.
//

import UIKit
import SideMenu
import Alamofire
import GoogleMaps
import GooglePlaces
import RaveSDK

class HomeViewController: UIViewController, UIGestureRecognizerDelegate {

    // Navigation
    @IBOutlet weak var navigationBarView: UIView!
    
    @IBOutlet weak var containerViews: UIView!
    
    
    @IBOutlet weak var operatorName: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var statusButon: UIButton!
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var accountBalance: UILabel!
    @IBOutlet weak var balanceAmount: UILabel!
    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var currencyButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var navigationButton: UIButton!
    
    @IBOutlet weak var mapViewBottomConstrain: NSLayoutConstraint!
    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeightConstrain: NSLayoutConstraint!
    var bottomViewConstrain: NSLayoutConstraint?
    
    
    //    @IBOutlet weak var currencyContainerView: UIView!
//    @IBOutlet weak var currencyViewHeightConstrain: NSLayoutConstraint!

    @IBOutlet weak var mapView: GMSMapView!
    
    // ViewModel
    var homeViewModel = HomeViewModel()
    var locationViewModel = LocationViewModel()
    
    var menu: SideMenuNavigationController?
    
    // Default Variables
    let greenColor = UIColor(red: 38/255, green: 217/255, blue: 117/255, alpha: 1)
    
    // UIView
    var searchLocationTableView: SearchLocationTableView?
    var mobileChargePointEnroute: MobileChargePointEnroute?
    var mobileChargePointOrderView: MobileChargePointOrderView?
    var dropDownMenuView: DropDownMenuView?
    
    // For Currency Dropdown
    let currencyView = UIView()
    var currencyHeightAnchor: NSLayoutConstraint?
    
    // Incoming Request
    var incomingView = UIView()
    var incomngHeightConstrain: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDesignStyle() 
        initialLoad()
        onClickButtonAction()
        setSideMenuController()
        
       // homeViewModel.postResult()
        homeViewModel.getre()
        
        // For MenuButton First action
        if #available(iOS 14.0, *) {
            currencyButtonAction()
        } else {
            //
        }

    }
    func initialLoad() {
        
        mapView.bringSubviewToFront(statusView)
        
        // Set google map
        locationViewModel.setGoogleMap(mapView: mapView)
        
        // Get CurrentLocation
        locationButtonAction()
        
        // For Currency DropDown
        currencyView.translatesAutoresizingMaskIntoConstraints = false
        currencyView.backgroundColor = .gray
        mapView.addSubview(currencyView)
        
        NSLayoutConstraint.activate([
            currencyView.widthAnchor.constraint(equalToConstant: 200),
            currencyView.trailingAnchor.constraint(equalTo: self.mapView.trailingAnchor, constant: -20),
            currencyView.bottomAnchor.constraint(equalTo: self.statusView.topAnchor, constant: 0)
        ])
        
//        incomingView.translatesAutoresizingMaskIntoConstraints = false
//        incomingView.backgroundColor = .gray
//        mapView.addSubview(incomingView)
//
//        NSLayoutConstraint.activate([
//            incomingView.bottomAnchor.constraint(equalTo: mapView.bottomAnchor, constant: 0),
//            incomingView.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: 0),
//            incomingView.leadingAnchor.constraint(equalTo: mapView.leadingAnchor , constant: 0)
//        ])
       
    }
    
    func setDesignStyle() {
        operatorName.setLabelCornerRadius(radius: 5)
        statusButon.setButtonRoundCornerWithShadow(style: .normal, radius: 20)
        statusView.setViewCornorRadiusWithShadow(radius: 10)
        balanceView.setViewCornorRadiusWithShadow(radius: 10)
        locationButton.setButtonRoundCornerWithShadow(style: .round, radius: 40)
        navigationButton.setButtonRoundCornerWithShadow(style: .round, radius: 40)
        currencyView.setViewCornerWithRadiusAndNoBorder(radius: 10)
    }
    
    func onClickButtonAction() {
        menuButton.addTarget(self, action: #selector(menuButtonTapped), for: .touchUpInside)
        filterButton.addTarget(self, action: #selector(filterButtonAction), for: .touchUpInside)
        statusButon.addTarget(self, action: #selector(statusButtonAction), for: .touchUpInside)
        locationButton.addTarget(self, action: #selector(locationButtonAction), for: .touchUpInside)
        navigationButton.addTarget(self, action: #selector(navigationButtonAction), for: .touchUpInside)
        currencyButton.addTarget(self, action: #selector(currencyButtonAction), for: .touchUpInside)
        profileButton.addTarget(self, action: #selector(profileButtonAction), for: .touchUpInside)
    }
    
    func setSideMenuController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sideMenuController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as? SideMenuViewController
        menu = SideMenuNavigationController(rootViewController: sideMenuController!)
        menu?.leftSide = true
        menu?.menuWidth = self.view.frame.width/1.3
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.rightMenuNavigationController = nil
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
    }
    
    @objc func menuButtonTapped() {
        dismissDropDownMenu()
        present(menu!, animated: true)
    }
    
    @objc func filterButtonAction() {
        dismissDropDownMenu()
        let controller = storyboard?.instantiateViewController(withIdentifier: "FilterSettingsViewController") as? FilterSettingsViewController
        navigationController?.pushViewController(controller!, animated: true)
    }
    
  
    
    @objc func statusButtonAction() {
        dismissDropDownMenu()
       //loadMobileChargePointOrderView()
        UIView.animate(withDuration: 0.4) {
            self.statusView.isHidden = true
            self.view.backgroundColor = .lightGray
            self.bottomView.backgroundColor = .orange
            self.bottomViewConstrain = self.bottomView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
           // self.bottomViewConstrain = self.bottomView.heightAnchor.constraint(equalToConstant:  400)
            self.bottomViewConstrain?.isActive = true
            self.view.layoutIfNeeded()
            self.loadMobileChargeEnroute()
        }
    }
    
    @objc func locationButtonAction() {
        locationViewModel.getCurrenctLocation { location in
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 10)
            self.mapView.animate(to: camera)
        }
        
    }
    
    @objc func navigationButtonAction() {
        dismissDropDownMenu()
    }
    
    @objc func profileButtonAction() {
                
    }
    
    @objc func currencyButtonAction() {
        if #available(iOS 14.0, *) {
            DropDownMenu.configureDropDownMenu(dataSource: Resource.currencyList, menuTitle: "Currency Lists", field: currencyButton) { title in
            }
        } else {
            // Fallback on earlier versions
            UIView.animate(withDuration: 0.2) {
                self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.6)
                self.currencyHeightAnchor?.isActive = true
                self.view.layoutIfNeeded()
                self.loadDropDownMenuView()

            }
        }
    }
 
}

// MARK:- SIDE MENU - Delegate
extension HomeViewController: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        operatorName.isHidden = true
        statusView.isHidden = true
        menuButton.setImage(UIImage(named: "cancel"), for: .normal)
        menuButton.frame.size = CGSize(width: 20, height: 20)
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
       operatorName.isHidden = false
        statusView.isHidden = false
        menuButton.setImage(UIImage(systemName: "line.3.horizontal.circle.fill"), for: .normal)
    }
}

// MARK:- ADD UIView
extension HomeViewController {
    
    func loadDropDownMenuView() {
        
        if dropDownMenuView == nil, let bundle = Bundle.main.loadNibNamed("DropDownMenuView", owner: self, options: nil)?.first as? DropDownMenuView {
            
            self.dropDownMenuView = bundle
       
            self.dropDownMenuView?.frame = CGRect(x: 0, y: 0, width: self.currencyView.frame.width, height: self.currencyView.frame.height)
            self.dropDownMenuView?.selectedValue = { name, status in
                self.currencyButton.setTitle(name, for: .normal)
                
                print("DropDown Status \(status)")
                if !status {
                    
                } else {
                    self.dismissDropDownMenu()
                }
            }
            
            self.currencyView.addSubview(self.dropDownMenuView!)
        }
    }
    
    func dismissDropDownMenu() {
        UIView.animate(withDuration: 0.2) {
            self.currencyHeightAnchor?.isActive = false
            self.currencyHeightAnchor = self.currencyView.heightAnchor.constraint(equalToConstant: 0)
            self.currencyHeightAnchor?.isActive = true
            
            self.view.layoutIfNeeded()
            self.dropDownMenuView?.titleView.isHidden = true
            
        } completion: { _ in
            self.removeDropDownMenuView()
            self.dropDownMenuView?.titleView.isHidden = false
        }
    }
    
    func removeDropDownMenuView() {
        self.dropDownMenuView?.removeFromSuperview()
        self.dropDownMenuView = nil
    }
    
    func loadMobileChargePointOrderView() {

        if mobileChargePointOrderView == nil, let bundle = Bundle.main.loadNibNamed("MobileChargePointOrderView", owner: self, options: nil)?.first as? MobileChargePointOrderView {

            self.mobileChargePointOrderView = bundle
            self.mobileChargePointOrderView?.frame = CGRect(x: 0, y: ((self.view.frame.origin.y+self.view.frame.size.height)-(self.mobileChargePointOrderView?.frame.size.height)! - 20), width: self.view.frame.size.width, height: self.mobileChargePointOrderView!.frame.size.height+20)

            self.mobileChargePointOrderView?.cancelButton.addTarget(self, action: #selector(OrderCancelButtonAction), for: .touchUpInside)
            self.mobileChargePointOrderView?.confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
            self.view.addSubview(self.mobileChargePointOrderView!)

        }
    }
    
//    func loadMobileChargePointOrderView() {
//
//        if mobileChargePointOrderView == nil, let bundle = Bundle.main.loadNibNamed("MobileChargePointOrderView", owner: self, options: nil)?.first as? MobileChargePointOrderView {
//
//            self.mobileChargePointOrderView = bundle
//            self.mobileChargePointOrderView?.frame = CGRect(x: 0, y: 0, width: self.incomingView.frame.size.width, height: self.mobileChargePointOrderView!.frame.size.height)
//
//            self.mobileChargePointOrderView?.cancelButton.addTarget(self, action: #selector(OrderCancelButtonAction), for: .touchUpInside)
//            self.mobileChargePointOrderView?.confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
//            self.incomingView.addSubview(self.mobileChargePointOrderView!)
//
//        }
//    }
    func removeMobileChargePointOrderView() {
        self.mobileChargePointOrderView?.removeFromSuperview()
        self.mobileChargePointOrderView = nil
    }
    
    @objc func confirmButtonAction() {
        removeMobileChargePointOrderView()
        //loadMobileChargeEnroute()
        
        UIView.animate(withDuration: 0.5) {
            self.view.backgroundColor = .lightGray
            self.bottomView.backgroundColor = .clear
            self.bottomViewConstrain = self.bottomView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.4)
            self.bottomViewConstrain?.isActive = true
            self.view.layoutIfNeeded()
            self.loadMobileChargeEnroute()
        }
    }
    
    @objc func OrderCancelButtonAction() {
        removeMobileChargePointOrderView()
    }
    
    func loadMobileChargeEnroute() {
        
        if mobileChargePointEnroute == nil, let bundle = Bundle.main.loadNibNamed("MobileChargePointEnroute", owner: self, options: nil)?.first as? MobileChargePointEnroute {
            
            self.mobileChargePointEnroute = bundle
    
            self.mobileChargePointEnroute?.frame = CGRect(x: 0, y: 0, width: self.bottomView.frame.size.width, height: self.bottomView.frame.size.height)
            
            self.mobileChargePointEnroute?.cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
            self.bottomView.addSubview(self.mobileChargePointEnroute!)
        }
        
    }
    
    @objc func cancelButtonAction() {
        UIView.animate(withDuration: 0.3) {
            self.statusView.isHidden = false
            self.bottomViewConstrain?.isActive = false
            self.bottomViewConstrain = self.bottomView.heightAnchor.constraint(equalToConstant: 0)
            self.bottomViewConstrain?.isActive = true
            self.mobileChargePointEnroute?.mainViewHeightConstrain.constant = 0
            self.mobileChargePointEnroute?.confirmedOrderLabel.isHidden = true
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
                self.removeMobileChargeEnroute()
            }
            
            
        }
    }
    
    func removeMobileChargeEnroute() {
        
        mobileChargePointEnroute?.removeFromSuperview()
        mobileChargePointEnroute = nil
    }
}

// MARK:- Set GoogleMap Style
extension HomeViewController: GMSMapViewDelegate {
    
    func setGoogleMapStyle() {
        
        do {
            if let styleUrl = Bundle.main.url(forResource: "MapStyle", withExtension: "json") {
                self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleUrl)
                print("Style loaded")
            } else {
                print("Unable to find style")
            }
        } catch {
            print("Map style failed to load \(error)")
        }
        
    }
}

