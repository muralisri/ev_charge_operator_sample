//
//  IncomingRequestViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 04/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit

class IncomingRequestViewController: UIViewController {
    
    @IBOutlet weak var animatView: UIView!
    
    @IBOutlet weak var animateViewHeight: NSLayoutConstraint!
    var mobileChargePointOrderView: MobileChargePointOrderView?
    var mobileChargePointEnroute: MobileChargePointEnroute?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        if let presentationController = presentationController as? UISheetPresentationController {
//            presentationController.detents = [
//                .medium()
//                                            ,
//                                            .large()
//            ]
//
//            presentationController.prefersScrollingExpandsWhenScrolledToEdge = true
//
//        }
        
        //self.loadMobileChargePointOrderView()
        //self.loadMobileChargeEnroute()
        
        animateViewHeight.constant = 0
        
        
        
    }
    @IBAction func backbutton(_ sender: Any) {
        
        animatView.isHidden = true
        animateViewHeight.constant = 0
        //animatView.removeFromSuperview()
        self.view.layoutIfNeeded()
    }
    
    @IBAction func buttonAction(_ sender: Any) {
       
        UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut) {
            
            self.animatView.isHidden = false
            //self.view.addSubview(self.animatView)
            ///self.animateViewHeight.constant = 500
            
            self.loadMobileChargePointOrderView()
            self.mobileChargePointOrderView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.height)
            
            self.view.layoutIfNeeded()
        } completion: { _ in
            
        }

    }
    
    func loadMobileChargePointOrderView() {
        
        if mobileChargePointOrderView == nil, let bundle = Bundle.main.loadNibNamed("MobileChargePointOrderView", owner: self, options: nil)?.first as? MobileChargePointOrderView {
            
            self.mobileChargePointOrderView = bundle
            self.mobileChargePointOrderView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 0)
            
            self.mobileChargePointOrderView?.cancelButton.addTarget(self, action: #selector(OrderCancelButtonAction), for: .touchUpInside)
            self.mobileChargePointOrderView?.confirmButton.addTarget(self, action: #selector(confirmButtonAction), for: .touchUpInside)
            
            self.view.addSubview(self.mobileChargePointOrderView!)
            
            // self.subview.addSubview(self.mobileChargePointOrderView!)
            
            //subview.addSubview(self.mobileChargePointOrderView!)
            
        }
    }
    
    func removeMobileChargePointOrderView() {
        self.mobileChargePointOrderView?.removeFromSuperview()
        self.mobileChargePointOrderView = nil
    }
    
    @objc func confirmButtonAction() {
        removeMobileChargePointOrderView()
        loadMobileChargeEnroute()
    }
    
    
    @objc func OrderCancelButtonAction() {
        removeMobileChargePointOrderView()
        
    }
    
    func loadMobileChargeEnroute() {
        
        if mobileChargePointEnroute == nil, let bundle = Bundle.main.loadNibNamed("MobileChargePointEnroute", owner: self, options: nil)?.first as? MobileChargePointEnroute {
            
            self.mobileChargePointEnroute = bundle
            self.mobileChargePointEnroute?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            // self.mobileChargePointEnroute?.frame = CGRect(x: 0, y: (self.view.frame.origin.y+self.view.frame.height)-((self.mobileChargePointOrderView?.frame.height)!), width: self.view.frame.width, height: (self.mobileChargePointOrderView?.frame.self.height)!)
            
            self.mobileChargePointEnroute?.cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
            self.view.addSubview(self.mobileChargePointEnroute!)
        }
        
    }
    
    @objc func cancelButtonAction() {
        removeMobileChargeEnroute()
    }
    
    func removeMobileChargeEnroute() {
        
        mobileChargePointEnroute?.removeFromSuperview()
        mobileChargePointEnroute = nil
    }
    
    
    
}
