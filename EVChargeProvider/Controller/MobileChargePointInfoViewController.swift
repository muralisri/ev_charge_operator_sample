//
//  MobileChargePointInfoViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 11/01/22.
//

import UIKit

class MobileChargePointInfoViewController: UIViewController {

    // Navigation
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navigationTitle: UILabel! 
    
    // Charge Point Name
    @IBOutlet weak var chargePointDetailsView: UIView!
    
    @IBOutlet weak var chargePointDetaileImage: UIImageView!
    @IBOutlet weak var chargePointName: UILabel!

    // Distance
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var cost: UILabel!
    
    // Charge Points
    @IBOutlet weak var chargePointsView: UIView!
    @IBOutlet weak var maxPowerRight: UILabel!
    @IBOutlet weak var maxPowerLeft: UILabel!
    
    // Operator Details
    @IBOutlet weak var operatorDetailsView: UIView!
    @IBOutlet weak var operatorProfileImage: UIImageView!
    @IBOutlet weak var operatorNameLable: UILabel!
    @IBOutlet weak var operatorName: UILabel!
    @IBOutlet weak var operatorPhoneNoLable: UILabel!
    @IBOutlet weak var operatorPhoneNo: UILabel!
    
    // Transcation Details
    @IBOutlet weak var transcationsDetailsView: UIView!
   
    // Transcations
    @IBOutlet weak var transcationsView: UIView!
    @IBOutlet weak var transcationsLable: UILabel!
    @IBOutlet weak var transcationCount: UILabel!
    @IBOutlet weak var transcationsPercentage: UILabel!
    
   // Energy
    @IBOutlet weak var energyUsedView: UIView!
    @IBOutlet weak var energyUsedLable: UILabel!
    @IBOutlet weak var energyCount: UILabel!
    @IBOutlet weak var energyPercentage: UILabel!
    
    //gCO@  saved
    @IBOutlet weak var gCO2UsedView: UIView!
    @IBOutlet weak var gCo2SavedLable: UILabel!
    @IBOutlet weak var gCo2SavedCount: UILabel!
    @IBOutlet weak var gCo2SavedPercentage: UILabel!
    
    @IBOutlet weak var scanQRCodeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad()
        setDesignStyle()

    }
    func initialLoad() {
        
    backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        scanQRCodeButton.addTarget(self, action: #selector(scanQRCodeButtonAction), for: .touchUpInside)
    }
    
    @objc func backButtonAction() {
        
        dismiss(animated: true)
    }
    
    func setDesignStyle() {
        
        chargePointDetailsView.setViewCornorRadiusWithShadow(radius: 10)
        chargePointDetaileImage.setImageCornorRadius(radius: 10)
        chargePointsView.setViewCornerWithRadiusAndNoBorder(radius: 10)
        operatorDetailsView.setViewCornorRadiusWithShadow(radius: 20)
        operatorProfileImage.setImageCornorRadius(radius: 20)
        
        transcationsDetailsView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        transcationsView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        energyUsedView.setViewCornerWithRadiusAndNoBorder(radius: 7)
        gCO2UsedView.setViewCornerWithRadiusAndNoBorder(radius: 7)
      
        energyUsedView.setViewCornerRadiusWithBorder(radius: 10, width: 2, color: .black)
        transcationsView.setViewCornerRadiusWithBorder(radius: 10, width: 2, color: .black)
        gCO2UsedView.setViewCornerRadiusWithBorder(radius: 10, width: 2, color: .black)
        scanQRCodeButton.setViewCornerWithRadiusAndNoBorder(radius: 7)
        
     
        
        
    }

    @objc func scanQRCodeButtonAction() {
        push(controller: "QrCodeScannerViewController", animated: true)
    }
    
    
}
