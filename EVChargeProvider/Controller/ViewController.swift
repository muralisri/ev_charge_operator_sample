//
//  ViewController.swift
//  EVChargeProvider
//
//  Created by MURALI on 19/02/22.
//  Copyright © 2022 Hepto. All rights reserved.
//

import UIKit
import FLAnimatedImage

class ViewController: UIViewController {

    @IBOutlet weak var loadingImage: FLAnimatedImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLodingGifAnimation()
    }
    func setLodingGifAnimation() {
        let path = Bundle.main.path(forResource: "AppLoading", ofType: "gif")
        let url = URL(fileURLWithPath: path!)
        let gifData = try? Data(contentsOf: url)
        let imageData = FLAnimatedImage(animatedGIFData: gifData)
        loadingImage.animatedImage = imageData
        
    }
    

}
